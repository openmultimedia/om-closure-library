/** @externs */

/** @type {Object} */
var JSON;

/**
 * Returns a serialized version of obj
 * @param {*} obj The object to serielize
 * @return {string} The serialized result
 * @nosideeffects
 */
JSON.stringify = function(obj) {};

/**
 * Return a object de-serialized from str
 * @param {string} str The serialized object
 * @return {*} The deserialized object
 * @nosideeffects
 */
JSON.parse = function(str) {};
