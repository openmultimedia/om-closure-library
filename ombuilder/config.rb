require 'ombuilder/closure/project_base'

module OMBuilder
  module ClosureProjectBase
    def use_om_closure_library
      add_externs_path relative_path_to(File.join(File.dirname(__FILE__),'../externs/'))
      add_root relative_path_to(File.join(File.dirname(__FILE__),'../omm/'))
    end
  end
end