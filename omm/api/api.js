goog.provide('omm.api');

/// Se definen los tipos básicos y Estructuras de Datos para el API REST

// Clip del API en forma de Record
/** @typedef {{metodo_preferido: string, archivo_url: string, streaming: { rtmp_server: string, rtmp_file: string }}} */
omm.api.Clip;

// Definición de un Registro de Programa
/** @typedef {{slug: string, nombre: string, descripcion: string, tipo: {descripcion: string, nombre: string}, imagen_url: string, creatv_id: number}} */
omm.api.Programa;

/** @typedef {{id: number, nombre: string, hora: {car: string, mad: string, bol: string, cub: string, bue: string, mex: string, bog: string}, hora_final: {car: string}, emision_id: number}} */
omm.api.HorarioPrograma;

/** @typedef {Object.<string, Array.<omm.api.HorarioPrograma>>} */
omm.api.HorarioSemana;

/**
 * Tipos de detalle de Clip
 * TODO (arv): Agregar todos los tipos de detalle
 * @enum {string}
 */
omm.api.DetalleClip = {
  COMPLETO: 'completo'
};

omm.api.TipoClip = {
  ENTREVISTA: 'entrevista'
};

omm.api.MetodoTransmision = {
  HTTP: 'http',
  STREAMING: 'streaming'
};
