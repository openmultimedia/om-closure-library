goog.provide('omm.api.utils');

goog.require('omm.api');

omm.api.utils.LOCALIZATION = {
  'es': {
    formatos: {
       fecha: '{dia-semana} {dia} de {mes}, {año}'
    },
    dias: [ 'Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado' ],
    meses: [ 'enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio',
      'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre' ]
  },
  'en': {
    formatos: {
       fecha: '{dia-semana} {mes} {dia}, {año}'
    },
    dias: [ 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday' ],
    meses: [ 'january', 'february', 'march', 'april', 'may', 'june',
       'july', 'august', 'september', 'october', 'november', 'december' ]
  },
  'pt': {
    formatos: {
       fecha: '{dia-semana} {dia} de {mes}, {año}'
    },
    dias: [ 'Domingo', 'Segunda-feira', 'Terça-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sábado' ],
    meses: [ 'janeiro', 'fevereiro', 'março', 'abril', 'maio', 'junho',
      'julho', 'agosto', 'setembro', 'outubro', 'novembro', 'dezembro' ]
  }
}

omm.api.utils.formatPlace = function (dataItem) {
  var etiqueta = '';
  var ciudad = dataItem['ciudad'];
  var estado = dataItem['estado'];
  var pais = dataItem['pais'];

  var pieces = [];

  if ( ciudad ) {
    pieces.push(ciudad);
  }

  if ( estado ) {
    pieces.push(estado['nombre']);
  }

  if ( pais ) {
    pieces.push(pais['nombre']);
  }

  return pieces.join(', ');
}

omm.api.utils.formatDateTime = function (dateString, lang) {
  var localization = omm.api.utils.LOCALIZATION[lang];

  var strFecha = null;

  var fechaPieces = /^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})$/.exec(dateString);
  if ( fechaPieces ) {
    var fechaJS = new Date(fechaPieces[1], fechaPieces[2] - 1, fechaPieces[3], fechaPieces[4], fechaPieces[5], fechaPieces[6]);

    var replaces = {
      'dia-semana': localization.dias[fechaJS.getDay()],
      'dia': fechaJS.getDate(),
      'mes': localization.meses[fechaJS.getMonth()],
      'año': fechaJS.getFullYear()
    };

    strFecha = localization.formatos.fecha;

    for ( var key in replaces ) {
      strFecha = strFecha.replace('{' + key + '}', replaces[key]);
    }
  }

  return strFecha;
};

omm.api.utils.formatDatePlace = function(dataItem, lang) {

  var strFecha = omm.api.utils.formatDateTime(dataItem['fecha'], lang);

  var strLugar = omm.api.utils.formatPlace(dataItem);

  if ( strFecha && strLugar ) {
    return strFecha + ' | ' + strLugar;
  }

  return strFecha || strLugar || '';
}

omm.api.utils.clipEsDeTipo = function (itemData, tipo) {
  return itemData['tipo'] && (itemData['tipo']['slug'] == tipo);
}
