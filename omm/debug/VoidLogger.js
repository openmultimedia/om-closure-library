goog.provide('omm.debug.VoidLogger');

goog.require('goog.debug.Logger');



/**
 * Crea un Logger vacío.
 * @constructor
 * @extends {goog.debug.Logger}
 */
omm.debug.VoidLogger = function() {
  //goog.base(this, 'omm.debug.VoidLogger');
};
//goog.inherits(omm.debug.VoidLogger, goog.debug.Logger);


/**
 * Log a message. If the logger is currently enabled for the
 * given message level then the given message is forwarded to all the
 * registered output Handler objects.
 * @param {goog.debug.Logger.Level} level One of the level identifiers.
 * @param {string} msg The string message.
 * @param {Error|Object=} opt_exception An exception associated with the
 *     message.
 */
omm.debug.VoidLogger.prototype.log =
    function(level, msg, opt_exception) {};


/**
 * Log a message at the Logger.Level.SHOUT level.
 * If the logger is currently enabled for the given message level then the
 * given message is forwarded to all the registered output Handler objects.
 * @param {string} msg The string message.
 * @param {Error=} opt_exception An exception associated with the message.
 */
omm.debug.VoidLogger.prototype.shout =
    function(msg, opt_exception) {};


/**
 * Log a message at the Logger.Level.SEVERE level.
 * If the logger is currently enabled for the given message level then the
 * given message is forwarded to all the registered output Handler objects.
 * @param {string} msg The string message.
 * @param {Error=} opt_exception An exception associated with the message.
 */
omm.debug.VoidLogger.prototype.severe =
    function(msg, opt_exception) {};


/**
 * Log a message at the Logger.Level.WARNING level.
 * If the logger is currently enabled for the given message level then the
 * given message is forwarded to all the registered output Handler objects.
 * @param {string} msg The string message.
 * @param {Error=} opt_exception An exception associated with the message.
 */
omm.debug.VoidLogger.prototype.warning =
    function(msg, opt_exception) {};


/**
 * Log a message at the Logger.Level.INFO level.
 * If the logger is currently enabled for the given message level then the
 * given message is forwarded to all the registered output Handler objects.
 * @param {string} msg The string message.
 * @param {Error=} opt_exception An exception associated with the message.
 */
omm.debug.VoidLogger.prototype.info =
    function(msg, opt_exception) {};


/**
 * Log a message at the Logger.Level.CONFIG level.
 * If the logger is currently enabled for the given message level then the
 * given message is forwarded to all the registered output Handler objects.
 * @param {string} msg The string message.
 * @param {Error=} opt_exception An exception associated with the message.
 */
omm.debug.VoidLogger.prototype.config =
    function(msg, opt_exception) {};


/**
 * Log a message at the Logger.Level.FINE level.
 * If the logger is currently enabled for the given message level then the
 * given message is forwarded to all the registered output Handler objects.
 * @param {string} msg The string message.
 * @param {Error=} opt_exception An exception associated with the message.
 */
omm.debug.VoidLogger.prototype.fine =
    function(msg, opt_exception) {};


/**
 * Log a message at the Logger.Level.FINER level.
 * If the logger is currently enabled for the given message level then the
 * given message is forwarded to all the registered output Handler objects.
 * @param {string} msg The string message.
 * @param {Error=} opt_exception An exception associated with the message.
 */
omm.debug.VoidLogger.prototype.finer =
    function(msg, opt_exception) {};


/**
 * Log a message at the Logger.Level.FINEST level.
 * If the logger is currently enabled for the given message level then the
 * given message is forwarded to all the registered output Handler objects.
 * @param {string} msg The string message.
 * @param {Error=} opt_exception An exception associated with the message.
 */
omm.debug.VoidLogger.prototype.finest =
    function(msg, opt_exception) {};

goog.addSingletonGetter(omm.debug.VoidLogger);
