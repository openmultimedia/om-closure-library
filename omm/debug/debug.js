goog.provide('omm.debug');

goog.require('goog.debug.Console');
goog.require('goog.debug.Logger');
goog.require('omm.debug.VoidLogger');
goog.require('goog.debug.FancyWindow');
goog.require('goog.format.JsonPrettyPrinter');

if (!COMPILED) {
  omm.debug.setDefine = function(path, value) {
    goog.exportSymbol(path, value);
  };
}

/**
 * @define {boolean} Define para {@code omm.debug.LOGGING_ENABLED}.
 */
omm.debug.LOGGING_ENABLED = goog.DEBUG;

omm.debug.isLoggingEnabled = function() {
  if (!omm.debug.isLoggingEnabled_) {
    omm.debug.isLoggingEnabled_ = omm.debug.LOGGING_ENABLED;
    omm.debug.setup(omm.debug.isLoggingEnabled_);
  }

  return omm.debug.isLoggingEnabled_;
};

/**
 * @define {string} Define para {@code omm.debug.DEBUG_LEVEL}.
 */
omm.debug.DEF_DEFAULT_DEBUG_LEVEL = '';


/**
 * Nivel de logging para los componentes de openmultimedia
 * @const
 * @type {string}
 */
omm.debug.DEFAULT_DEBUG_LEVEL =
    ((!COMPILED) && goog.global['omm.debug.DEFAULT_DEBUG_LEVEL']) ||
    omm.debug.DEF_DEFAULT_DEBUG_LEVEL ||
    (goog.DEBUG ? 'WARNING' : 'NONE');


/**
 * Description
 * @param {string} name Nombre del Logger.
 * @param {object=} opt_obj Objecto específico que loggea
 * @param {string=} opt_level Nivel de logging.
 * @return {goog.debug.Logger} El Logger con dicho nombre y en ese nivel.
 */
omm.debug.getLogger = function(logger_name, opt_obj, opt_level) {
  if (! omm.debug.isLoggingEnabled() ) {
    var VoidLogger = {
        log: function(level, msg, opt_exception) {},
        shout: function(msg, opt_exception) {},
        severe: function(msg, opt_exception) {},
        warning: function(msg, opt_exception) {},
        info: function(msg, opt_exception) {},
        config: function(msg, opt_exception) {},
        fine: function(msg, opt_exception) {},
        finer: function(msg, opt_exception) {},
        finest: function(msg, opt_exception) {}
    };

    return VoidLogger;
  }

  var name = logger_name;

  if ( opt_obj ) {
    name = name + '[' + goog.getUid(opt_obj) +']';
  }

  if (! omm.debug.loggers_[name]) {
    var logger = goog.debug.Logger.getLogger(name);

    var level = opt_level || omm.debug.DEFAULT_DEBUG_LEVEL;

    if (logger && opt_level) {
      var aplicable_level;

      if (/^\d+$/.test(opt_level)) {
        aplicable_level = parseInt(opt_level, 10);
      } else {
        aplicable_level = goog.debug.Logger.Level.getPredefinedLevel(opt_level);
      }

      logger.setLevel(aplicable_level);
    }

    omm.debug.loggers_[name] = logger;
  }

  return omm.debug.loggers_[name];
};

omm.debug.formatJson = function(jsonObject) {
  var formatter = omm.debug.getJsonFormatter_();
  var jsonString = formatter.format(jsonObject);

  if ( ! jsonString ) {
    switch (typeof jsonObject) {
        case 'object':
            jsonString = '(null)';
            break;
        case 'undefined':
            jsonString = '(undefined)';
            break;
        case 'string':
            jsonString = '(empty string)';
            break;
    }
  }

  return jsonString;
}

/**
 * JSON Formatter a utilizar para los mensajes de debug
 * @type {goog.format.JsonPrettyPrinter}
 */
omm.debug.jsonFormatter_ = null;

/**
 * Obtiene el Formatter actual para los mensajes de debug. Crea el Formatter si
 * no ha sido inicializado
 * @return {goog.format.JsonPrettyPrinter} El formatter
 */
omm.debug.getJsonFormatter_ = function() {
  if ( omm.debug.jsonFormatter_ == null ) {
    omm.debug.jsonFormatter_ = new goog.format.JsonPrettyPrinter();
  }

  return omm.debug.jsonFormatter_;
}

/**
  * Registro de Loggers creados
  * @type {array.<goog.debug.Logger>}
  * @private
  */
omm.debug.loggers_ = [];

omm.debug.setup = function(enabled) {
  if (enabled) {
    omm.debug.enableLogger = function() {
      omm.debug.loggerWindow_.setEnabled(true);
    }

    omm.debug.disableLogger = function() {
      omm.debug.loggerWindow_.setEnabled(false);
    }

    // Install the console, this intercepts logging calls
    // and forwards to the browser console.
    //var c = new goog.debug.Console();
    //c.setCapturing(true);

    omm.debug.loggerWindow_ = new goog.debug.FancyWindow('om-debug-window');
    omm.debug.loggerWindow_.setCapturing(true);

    goog.exportSymbol('logger.enable', omm.debug.enableLogger);
    goog.exportSymbol('logger.disable', omm.debug.disableLogger);
  }
};

goog.debug.createInstanceLoggerGetter = function(name) {
  return function() {
    return omm.debug.getLogger(name, this);
  }
};

goog.debug.createStaticLoggerGetter = function(name) {
  return function() {
    return omm.debug.getLogger(name);
  }
};
