goog.provide('omm.dom');

goog.require('goog.dom');
goog.require('goog.events');

omm.dom.scriptTarget_ = null;

omm.dom.getScriptTarget_ = function(opt_node) {
  if ( ! omm.dom.scriptTarget_ ) {
    var dh = goog.dom.getDomHelper(opt_node);

    var head = dh.getElementsByTagNameAndClass('head')[0];

    // In opera documents are not guaranteed to have a head element, thus we
    // have to make sure one exists before using it.
    if (!head) {
      var body = dh.getElementsByTagNameAndClass('body')[0];
      head = dh.createDom('head');
      body.parentNode.insertBefore(head, body);
    }

    omm.dom.scriptTarget_ = head;
  }

  return omm.dom.scriptTarget_;
};

omm.dom.attachStylesheetTag = function(src, opt_loadCallback, opt_abortCallback) {
  var headTag = omm.dom.getScriptTarget_();

  var stylesheetTag = goog.dom.createDom('link', {
      'rel': 'stylesheet',
      'type' : 'text/css',
      'href': src
  });

  var callback = goog.partial(
      omm.dom.onScriptLoadedCallback_,
      headTag, stylesheetTag, false, opt_loadCallback, opt_abortCallback
  );

  goog.events.listen(stylesheetTag, 'load', callback);

  goog.events.listen(stylesheetTag, 'readystatechange', callback);

  goog.dom.insertChildAt(headTag, stylesheetTag);
};

omm.dom.attachScriptTag = function(mode, srcOrText, opt_removeOnLoad, opt_loadCallback, opt_abortCallback) {
  var headTag = omm.dom.getScriptTarget_();

  var scriptTag;

  if ( mode == omm.dom.ScriptTagMode.Src) {
      scriptTag = goog.dom.createDom('script', {
          'type' : 'text/javascript',
          'src': srcOrText
      });
  } else { // TODO: test if mode is valid
      scriptTag = goog.dom.createDom('script',
          {'type' : 'text/javascript'},
          goog.dom.createTextNode(srcOrText)
      );
  }

  var callback = goog.partial(
      omm.dom.onScriptLoadedCallback_,
      headTag, scriptTag, !!opt_removeOnLoad, opt_loadCallback, opt_abortCallback
  );

  goog.events.listen(scriptTag, 'load', callback);

  goog.events.listen(scriptTag, 'readystatechange', callback);

  goog.dom.insertChildAt(headTag, scriptTag);
}

omm.dom.onScriptLoadedCallback_ = function(headTag, scriptTag, removeOnLoad, loadCallback, abortCallback, event, isAbort) {
    if ( isAbort || !scriptTag.readyState || /loaded|complete/.test( scriptTag.readyState ) ) {

        goog.events.removeAll(scriptTag, 'load');
        goog.events.removeAll(scriptTag, 'readystatechange');

        // Remove the script
        if ( removeOnLoad && headTag && scriptTag.parentNode ) {
            headTag.removeChild( scriptTag );
        }

        // Dereference the script
        scriptTag = undefined;

        if ( ! isAbort ) {
            if ( loadCallback )
                loadCallback();
        } else {
            if ( abortCallback )
                abortCallback();
        }
    }
};

omm.dom.ScriptTagMode = {
    Src: 'src',
    Text: 'text'
};
