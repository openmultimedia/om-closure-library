goog.provide('omm.externals.ExternalsManager');

goog.require('goog.events.EventTarget');
goog.require('omm.dom');
goog.require('omm.debug');

/**
 * Manejador para recursos externos (scripts)
 * @constructor
 * @extends {goog.events.EventTarget}
 */
omm.externals.ExternalsManager = function() {
    this.logger_ = new omm.debug.getLogger('omm.externals.ExternalsManager', this);

    this.loaded_ = false;
    this.loading_ = false;

    this.pendingQueue_ = [];
    this.pending_ = {};
    this.included_ = {};

    this.requirementsManager_ = null;

    goog.base(this);
};
goog.inherits(omm.externals.ExternalsManager, goog.events.EventTarget);

omm.externals.ExternalsManager.prototype.isLoaded = function() {
    return this.loaded_;
};

omm.externals.ExternalsManager.prototype.isLoading = function() {
    return this.loading_ || this.requirementsLoading_;
};

omm.externals.ExternalsManager.prototype.getIncludes = function() {
  var included = [];

  for (var k in this.included_) {
    if (this.included_.hasOwnProperty(k)) {
      included.push(k);
    }
  }

  return included;
};

omm.externals.ExternalsManager.prototype.include = function(name) {
    if (name instanceof omm.externals.ExternalsManager) {
      var includes = name.getIncludes();

      for (var j = 0; j < includes.length; j += 1) {
        this.include(includes[j]);
      }

      return;
    }

    if (name in this.included_) {
        //TODO: Already loaded, tests
        return;
    }

    if ( this.loaded_ || this.loading_ ) {
        throw new Error('Ya se han cargado los externos para este manager');
    }

    if ( ! omm.externals.ExternalsManager.isRegistered(name) ) {
        throw new Error('El externo solicitado no ha sido registrado');
    }

    if (omm.externals.ExternalsManager.hasRequirements(name)) {
        var requirements = omm.externals.ExternalsManager.getRequirements(name);
        if (!this.requirementsManager_) {
            this.requirementsManager_ = new omm.externals.ExternalsManager();
        }

        for (var i = 0; i < requirements.length; i += 1) {
            this.requirementsManager_.include(requirements[i]);
        }
    }

    this.included_[name] = true;
    this.pending_[name] = true;
    this.pendingQueue_.push(name);
}

omm.externals.ExternalsManager.prototype.onRequirementsLoaded_ = function(event) {
    this.requirementsLoaded_ = true;
    this.requirementsLoading_ = false;
    this.requirementsManager_ = null;
    this.load();
};

omm.externals.ExternalsManager.prototype.load = function() {
    if (!this.requirementsManager_) {
        // No hay requeriminientos
        this.requirementsLoading_ = false;
        this.requirementsLoaded_ = true;
    };

    if (!this.requirementsLoaded_ && !this.requirementsLoading_) {
        // Los requerimientos no se han cargado ni se estan cargando en este momento
        this.requirementsLoading_ = true;
        goog.events.listen(this.requirementsManager_, 'load', goog.bind(this.onRequirementsLoaded_, this));
        this.requirementsManager_.load();
        return;
    };

    if(this.requirementsLoading_) {
        // Los requerimientos se estan cargando en este momento
        throw new Error('Loas requerimientos se están cargando');
        return;
    }

    // if (this.requirementsLoaded_) { Los requerimientos ya se cargaron, continua }

    if ( this.loading_ || this.loaded_ ) {
        if (this.loading_)
            throw new Error('Ya se esta cargando');

        if (this.loaded_)
            throw new Error('Ya se cargó');

        return;
    }

    this.loading_ = true;
    for ( var i = 0; i < this.pendingQueue_.length; i += 1) {
        omm.externals.ExternalsManager.load(
            this.pendingQueue_[i],
            goog.bind(
                omm.externals.ExternalsManager.prototype.onLoaded_,
                this,
                this.pendingQueue_[i]
            )
        );
    }

    this.pendingQueue_ = null;
};

omm.externals.ExternalsManager.prototype.onLoaded_ = function(name, event) {
    delete this.pending_[name];

    if ( goog.object.isEmpty(this.pending_) ) {
        this.loaded_ = true;
        this.dispatchEvent('load');
        // This will not throw any more load events
        goog.events.removeAll(this, 'load');
    }
};

omm.externals.ExternalsManager.prototype.ensure = function(callback) {
  if (this.isLoaded()) {
    callback();
  } else {

    goog.events.listen(this, 'load', callback);

    if (!this.isLoading()) {
      this.load();
    }
  }
};

omm.externals.ExternalsManager.registered_ = {};
omm.externals.ExternalsManager.loading_ = {};
omm.externals.ExternalsManager.loaded_ = {};

omm.externals.ExternalsManager.register = function(name, url, testCallback, requirements) {
    if (name in omm.externals.ExternalsManager.registered_) {
        throw new Error('El externo de nombre ' + name + ' ya ha sido registrado.');
    }

    omm.externals.ExternalsManager.registered_[name] = {url: url, testCallback: testCallback, requirements: requirements, callbacks: []};
};

omm.externals.ExternalsManager.isRegistered = function(name) {
    return ( name in omm.externals.ExternalsManager.registered_);
};

omm.externals.ExternalsManager.hasRequirements = function(name) {
    return ((name in omm.externals.ExternalsManager.registered_)
        && omm.externals.ExternalsManager.registered_[name].requirements
        && (omm.externals.ExternalsManager.registered_[name].requirements.length > 0));
};

omm.externals.ExternalsManager.getRequirements = function(name) {
    var requirements;

    if (name in omm.externals.ExternalsManager.registered_) {
        requirements = omm.externals.ExternalsManager.registered_[name].requirements;
    }

    return requirements;
};

omm.externals.ExternalsManager.load = function(name, callback) {
    if ( ! omm.externals.ExternalsManager.isRegistered(name) ) {
        //TODO: throw error
        throw new Error('El externo de nombre ' + name + ' no ha sido registrado.');
    }

    if ( name in omm.externals.ExternalsManager.loaded_ ) {
        callback();
    } else {
        omm.externals.ExternalsManager.registered_[name].callbacks.push(callback);
    }

    if ( !(name in omm.externals.ExternalsManager.loading_) ) {

        if ( omm.externals.ExternalsManager.registered_[name].testCallback() ) {
            omm.externals.ExternalsManager.onLoaded_(name, null);
        } else {
            omm.externals.ExternalsManager.loading_[name] = true;

            var url = omm.externals.ExternalsManager.registered_[name].url;

            if (goog.isFunction(url)) {
                url(goog.partial(omm.externals.ExternalsManager.onLoaded_, name))
            } else {
                omm.externals.ExternalsManager.loadScriptFile(
                    omm.externals.ExternalsManager.registered_[name].url,
                    goog.partial(omm.externals.ExternalsManager.onLoaded_, name),
                    null // TODO: Include onAbort?
                );
            }
        }
    }
};

omm.externals.ExternalsManager.loadScriptFile = function(scriptUrl, loadCallback, errorCallback) {
    omm.dom.attachScriptTag(
        omm.dom.ScriptTagMode.Src,
        scriptUrl,
        false,
        loadCallback,
        errorCallback
    );
};

omm.externals.ExternalsManager.onLoaded_ = function(name, event) {
    if ( name in omm.externals.ExternalsManager.loaded_ ) {
        return;
    }

    omm.externals.ExternalsManager.loaded_[name] = true;

    if ( name in omm.externals.ExternalsManager.loading_) {
        delete omm.externals.ExternalsManager.loading_[name];
    }

    var callbacks = omm.externals.ExternalsManager.registered_[name].callbacks;

    omm.externals.ExternalsManager.registered_[name].callbacks = null;

    for ( var i = 0; i < callbacks.length; i += 1 ) {
        callbacks[i]();
    }
};
