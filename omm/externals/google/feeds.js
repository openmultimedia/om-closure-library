goog.provide('omm.externals.google.feeds');
goog.provide('omm.externals.google.Loader.feeds');

goog.require('omm.externals.google.loader');
goog.require('omm.externals.ExternalsManager');

omm.externals.google.feeds.ExternalId = 'google.feeds';

/**
 Options as in
 https://developers.google.com/feed/v1/devguide#load_the_javascript_api_and_ajax_search_module
*/
omm.externals.google.feeds.registerExternal = function (opt_version, opt_options) {
  var version = opt_version || 1;
  var options = opt_options || {};

  omm.externals.ExternalsManager.register(
    omm.externals.google.feeds.ExternalId,
    function(){ google.load('feeds', version, options); },
    function(){ return ((typeof google != 'undefined') && google.feeds); },
    [ omm.externals.google.loader.registerExternal() ]
  );
};
