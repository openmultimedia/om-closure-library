goog.provide("omm.externals.google.loader");

omm.externals.google.loader.ExternalId = 'google.loader';

omm.externals.google.loader.registerExternal = function() {
  if (!omm.externals.ExternalsManager.isRegistered(omm.externals.google.loader.ExternalId)) {

    omm.externals.ExternalsManager.register(
      omm.externals.google.loader.ExternalId,
      'https://www.google.com/jsapi',
      function() { return ((typeof google != 'undefined') && (google.load)); }
    );
  }

  return omm.externals.google.loader.ExternalId;
};
