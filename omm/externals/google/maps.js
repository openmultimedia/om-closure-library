goog.provide('omm.externals.google.maps');

goog.require('omm.externals.ExternalsManager');
goog.require('omm.externals.google.loader');

/** @define {boolean} Define si se usará el sensor para Google Maps o no */
omm.externals.google.maps.DEFAULT_USE_SENSOR = false;

omm.externals.google.maps.included_ = false;

omm.externals.google.maps.ExternalId = 'google.maps';

omm.externals.google.maps.registerExternal = function(opt_version, opt_options) {
  var externalId = omm.externals.google.maps.ExternalId;

  if (!omm.externals.ExternalsManager.isRegistered(externalId)) {
    var version = opt_version || 3;

    var options = {'other_params': 'sensor=' + (omm.externals.google.maps.DEFAULT_USE_SENSOR ? 'true' : 'false')};

    goog.object.extend(options, opt_options || {});

    omm.externals.ExternalsManager.register(
      externalId,
      function(callback){
        goog.object.extend(options, {'callback': callback});
        google.load('maps', version, options);
      },
      function(){ return ((typeof google != 'undefined') && google.maps); },
      [ omm.externals.google.loader.registerExternal() ]
    );
  }

  return externalId;
};

omm.externals.google.maps.makeMarkerImage = function (markerDescriptor, opt_basePath) {
  return new google.maps.MarkerImage(
    opt_basePath ? markerDescriptor.url.replace('{base-path}', opt_basePath): markerDescriptor.url,
    omm.externals.google.maps.makeSize( markerDescriptor.size ),
    omm.externals.google.maps.makePoint( markerDescriptor.origin ),
    omm.externals.google.maps.makePoint( markerDescriptor.anchor )
  );
};

omm.externals.google.maps.makeIcon = function (iconDescriptor) {
  var icon = {};

  icon['anchor'] = iconDescriptor.anchor &&
      omm.externals.google.maps.makePoint(iconDescriptor.anchor);

  icon['origin'] =  iconDescriptor.origin &&
    omm.externals.google.maps.makePoint(iconDescriptor.origin);

  icon['size'] = iconDescriptor.size &&
      omm.externals.google.maps.makeSize(iconDescriptor.size);

  icon['scaledSize'] = iconDescriptor.scaledSize &&
    omm.externals.google.maps.makeSize(iconDescriptor.scaledSize);

  icon['url'] = iconDescriptor.url;

  return icon;
};

omm.externals.google.maps.makeLatLng = function(latLngDescriptor) {
  return new google.maps.LatLng(latLngDescriptor.lat, latLngDescriptor.lng);
};

omm.externals.google.maps.makeLatLngBounds = function(latLngBoundsDescriptor) {
    return new google.maps.LatLngBounds(
        omm.externals.google.maps.makeLatLng(latLngBoundsDescriptor.sw)
        ,omm.externals.google.maps.makeLatLng(latLngBoundsDescriptor.ne)
    )
}

omm.externals.google.maps.makePolygon = function(polygonDescriptor) {
  var coords = polygonDescriptor.path;

  var path = [];
  for (var i = 0; i < coords.length; i += 1) {
    path[i] = omm.externals.google.maps.makeLatLng(coords[i]);
  }

  polygonDescriptor.path = path;

  return new google.maps.Polygon( polygonDescriptor );
}

omm.externals.google.maps.makeSize = function(sizeDescriptor) {
  return new google.maps.Size(sizeDescriptor.width, sizeDescriptor.height);
};

omm.externals.google.maps.makePoint = function(pointDescriptor) {
  return new google.maps.Point(pointDescriptor.x, pointDescriptor.y);
};

omm.externals.google.maps.makeMarkerShape = function(shapeDescriptor) {
  var shape = {
    'coord': shapeDescriptor.coord,
    'type': shapeDescriptor.type
  };

  return shape;
};


omm.externals.google.maps.MapTypeControlStyle = {
  DEFAULT: 0,
  HORIZONTAL_BAR: 1,
  DROPDOWN_MENU: 2
};

omm.externals.google.maps.ControlPosition = {
  TOP_LEFT: 1,
  TOP: 2,
  TOP_CENTER: 2,
  TOP_RIGHT: 3,
  LEFT_CENTER: 4,
  LEFT_TOP: 5,
  LEFT: 5,
  LEFT_BOTTOM: 6,
  RIGHT_TOP: 7,
  RIGHT: 7,
  RIGHT_CENTER: 8,
  RIGHT_BOTTOM: 9,
  BOTTOM_LEFT: 10,
  BOTTOM: 11,
  BOTTOM_CENTER: 11,
  BOTTOM_RIGHT: 12,
  CENTER: 13
};

omm.externals.google.maps.ZoomControlStyle = {
  DEFAULT:0,
  SMALL:1,
  LARGE:2,
  Wl:3,
  ANDROID:4
};
