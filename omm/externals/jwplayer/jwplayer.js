/**
 * @fileoverview Biblioteca para trabajar con el componente externo "jwplayer"
 * Contiene las rutas estáticas a la biblioteca JavaScript, al componente SWF y
 * la función global para inyectar la dependencia de la biblioteca JavaScript.
 * @author Alan Reyes (kg.designer@gmail.com)
 */
goog.provide('omm.externals.jwplayer');

goog.require('omm.resources.externals.jwplayer');
goog.require('omm.externals.ExternalsManager');
goog.require('omm.uri');

/**
 * Variable global para identificar cuando la biblioteca JavaScript de jwPlayer
 * ya ha sido cargada.
 * @type {boolean}
 */
omm.externals.jwplayer.included_ = false;

/**
 * @define {string}
 */
omm.externals.jwplayer.HTTPS_JS_PATH = '';

/**
 * @define {string} 
 */
omm.externals.jwplayer.HTTP_JS_PATH = '';

omm.externals.jwplayer.jsUrl_ = '';

omm.externals.jwplayer.getJsUrl = function() {
  if (!omm.externals.jwplayer.jsUrl_) {
    omm.externals.jwplayer.jsUrl_ = omm.uri.resolveUrl(
      omm.externals.jwplayer.HTTPS_JS_PATH,
      omm.externals.jwplayer.HTTP_JS_PATH);
  }

  return omm.externals.jwplayer.jsUrl_;
};

/**
 * @define {string} ./jwplayer/player.swf
 */
omm.externals.jwplayer.HTTPS_SWF_PATH = '';

/**
 * @define {string} ./jwplayer/player.swf
 */
omm.externals.jwplayer.HTTP_SWF_PATH = '';

omm.externals.jwplayer.swfUrl_ = '';

omm.externals.jwplayer.getSwfUrl = function() {
  if (!omm.externals.jwplayer.swfUrl_) {
    omm.externals.jwplayer.swfUrl_ = omm.uri.resolveUrl(
      omm.externals.jwplayer.HTTPS_SWF_PATH,
      omm.externals.jwplayer.HTTP_SWF_PATH);
  }

  return omm.externals.jwplayer.swfUrl_;
};

/**
 * Valores posibles para el modo de funcionamiento del reproductor
 * @enum {string}
 */
omm.externals.jwplayer.RenderingMode = {
  /** El reproductor está funcionando en modo html5 */
  Html5: 'html5',
  /** El reproductor esta funcionando en modo flash */
  Flash: 'flash',
  /** El reproductor está funcionando en modo download */
  Download: 'download'
}

/**
 * Posiciones posibles de un Plugin
 * @enum {string}
 */
omm.externals.jwplayer.PluginPosition = {
  Dock: 'dock',
  Controlbar: 'controlbar'
};

omm.externals.jwplayer.ExternalId = 'jwplayer';

omm.externals.jwplayer.registerExternal = function() {
  omm.externals.ExternalsManager.register(
      omm.externals.jwplayer.ExternalId,
      omm.externals.jwplayer.getJsUrl(),
      function() { return (typeof jwplayer != 'undefined'); }
  );

  return omm.externals.jwplayer.ExternalId;
}
