goog.provide('omm.externals.twitter.TweetCache');

omm.externals.twitter.TweetCache = function(opt_options) {
  this.setOptions(opt_options);

  this.tweetsByScreenName_ = {};
  this.tweetsById_ = {};
};

omm.externals.twitter.TweetCache.prototype.options_ = {
  'timeout': 60 * 5
}

omm.externals.twitter.tweetsByScreenName_ = null;

omm.externals.twitter.tweetsById_ = null;

omm.externals.twitter.TweetCache.prototype.getTweets = function (key) {};

omm.externals.twitter.TweetCache.prototype.setTweets = function(key, tweets) {
  
}