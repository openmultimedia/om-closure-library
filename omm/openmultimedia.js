goog.provide('openmultimedia');

/** @define {string} DEFINE para {@code omm.BASE_PATH} */
omm.DEF_BASE_PATH = '';

/**
 * Ruta base a la carpeta con el contenido de openmultimedia (o al proyecto)
 * @const
 * @type {string}
 */
omm.BASE_PATH =
  ((!COMPILED) && goog.global['omm.OPENMULTIMEDIA_BASEPATH']) ||
  omm.DEF_BASE_PATH ||
  ''; // La ruta por defaultes la raíz del servidor
