goog.provide('omm.resources.externals');

goog.require('omm.resources');

/** @define {string} Define para {@code omm.resources.externals.DIR_NAME} */
omm.resources.externals.DEF_DIR_NAME = '';

/** @define {string} Define para {@code omm.resources.externals.DIR_PATH} */
omm.resources.externals.DEF_DIR_PATH = '';

/**
 * Nombre base del directorio donde se encuentran los recursos de bibliotecas externas
 * @const
 * @type {string}
 */
omm.resources.externals.DIR_NAME =
  ((!COMPILED) && goog.global['OPENMULTIMEDIA_RESOURCES_EXTERNALS_DIRNAME']) ||
  omm.resources.externals.DEF_DIR_NAME ||
  'externals';

/**
 * Ruta del directorio donde se encuentran los recursos de bibliotecas externas
 * @const
 * @type {string}
 */
omm.resources.externals.DIR_PATH =
  ((!COMPILED) && goog.global['OPENMULTIMEDIA_RESOURCES_EXTERNALS_DIRPATH']) ||
  omm.resources.externals.DEF_DIR_PATH ||
  (omm.resources.DIR_PATH + '/' + omm.resources.externals.DIR_NAME);