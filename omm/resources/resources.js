goog.provide('omm.resources');

goog.require('openmultimedia');

/** @define {string} DEFINE para {@code omm.resources.DIR_NAME} */
omm.resources.DEF_DIR_NAME = '';

/** @define {string} DEFINE para {@code omm.resources.DIR_PATH} */
omm.resources.DEF_DIR_PATH = '';

/**
 * Nombre del directorio base para los recursos de openmultimedia
 * @const
 * @type {string}
 */
omm.resources.DIR_NAME =
  ((!COMPILED) && goog.global['OPENMULTIMEDIA_RESOURCES_DIRNAME']) ||
  omm.resources.DEF_DIR_NAME ||
  'resources';

/**
 * Ruta del directorio base para los recursos de openmultimedia
 * @const
 * @type {string}
 */
omm.resources.DIR_PATH =
    ((!COMPILED) && goog.global['OPENMULTIMEDIA_RESOURCES_DIRPATH']) ||
    omm.resources.DEF_DIR_PATH ||
    (omm.BASE_PATH + '/' + omm.resources.DIR_NAME);
