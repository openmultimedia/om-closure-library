goog.provide('omm.resources.video.jwplugin');

goog.require('omm.resources.video');

/** @define {string} Define para {@code omm.resources.video.jwplugin.DEF_DEBUG} */
omm.resources.video.jwplugin.DEF_DEBUG = '';

/** @define {string} Define para {@code omm.resources.video.jwplugin.DIR_NAME} */
omm.resources.video.jwplugin.DEF_DIR_NAME = '';

/** @define {string} Define para {@code omm.resources.video.jwplugin.DIR_PATH} */
omm.resources.video.jwplugin.DEF_DIR_PATH = '';

/** @define {string} Define para {@code omm.resources.video.jwplugin.MULTIMEDIA_PLUGIN_NAME} */
omm.resources.video.jwplugin.DEF_MULTIMEDIA_PLUGIN_NAME = '';

/** @define {string} Define para {@code omm.resources.video.jwplugin.MULTIMEDIA_PLUGIN_FILENAME} */
omm.resources.video.jwplugin.DEF_MULTIMEDIA_PLUGIN_FILENAME = '';

/** @define {string} Define para {@code omm.resources.video.jwplugin.MULTIMEDIA_PLUGIN_PATH} */
omm.resources.video.jwplugin.DEF_MULTIMEDIA_PLUGIN_PATH = '';

/**
 * Indica si se usarán las versiones DEBUG o RELEASE del plugin
 * @const
 * @type {boolean}
 */
omm.resources.video.jwplugin.DEBUG =
    ((!COMPILED) && goog.global['OPENMULTIMEDIA_RESOURCES_VIDEO_JWPLUGIN_DEBUG']) ||
    omm.resources.video.jwplugin.DEF_DEBUG ||
    goog.DEBUG;

/**
 * Nombre base del directorio con los recursos para el Plugin de jwPlayer
 * @const
 * @type {string}
 */
omm.resources.video.jwplugin.DIR_NAME =
    ((!COMPILED) && goog.global['OPENMULTIMEDIA_RESOURCES_VIDEO_JWPLUGIN_DIRNAME']) ||
    omm.resources.video.jwplugin.DEF_DIR_NAME ||
    'jwplugin';

/**
 * Ruta del directorio con los recursos para el Plugin de jwPlayer
 * @const
 * @type {string}
 */
omm.resources.video.jwplugin.DIR_PATH =
  ((!COMPILED) && goog.global['OPENMULTIMEDIA_RESOURCES_VIDEO_JWPLUGIN_DIRPATH']) ||
  omm.resources.video.jwplugin.DEF_DIR_PATH ||
  (omm.resources.video.DIR_PATH + '/' + omm.resources.video.jwplugin.DIR_NAME);

/**
 * Nombre base del Plugin de jwPlayer
 * @const
 * @type {string}
 */
omm.resources.video.jwplugin.MULTIMEDIA_PLUGIN_NAME =
    ((!COMPILED) && goog.global['OPENMULTIMEDIA_RESOURCES_VIDEO_JWPLUGIN_MULTIMEDIAPLUGINNAME']) ||
    omm.resources.video.jwplugin.DEF_MULTIMEDIA_PLUGIN_NAME ||
    (omm.resources.video.jwplugin.DEBUG ? 'openmultimedia' : 'omm.min');

/**
 * Nombre de archivo base del Plugin de jwPlayer
 * @const
 * @type {string}
 */
omm.resources.video.jwplugin.MULTIMEDIA_PLUGIN_FILENAME =
    ((!COMPILED) && goog.global['OPENMULTIMEDIA_RESOURCES_VIDEO_JWPLUGIN_MULTIMEDIAPLUGINFILENAME']) ||
    omm.resources.video.jwplugin.DEF_MULTIMEDIA_PLUGIN_FILENAME ||
    (omm.resources.video.jwplugin.MULTIMEDIA_PLUGIN_NAME + '.js');

/**
 * Ruta del archivo del Plugin de jwPlayer
 * @const
 * @type {string}
 */
omm.resources.video.jwplugin.MULTIMEDIA_PLUGIN_PATH =
  ((!COMPILED) && goog.global['OPENMULTIMEDIA_RESOURCES_VIDEO_JWPLUGIN_MULTIMEDIAPLUGINPATH']) &&
  omm.resources.video.jwplugin.DEF_MULTIMEDIA_PLUGIN_PATH ||
  (omm.resources.video.jwplugin.DIR_PATH + '/' + omm.resources.video.jwplugin.MULTIMEDIA_PLUGIN_FILENAME);
