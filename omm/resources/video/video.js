goog.provide('omm.resources.video');

goog.require('omm.resources');

/** @define {string} Define para {@code omm.resources.video.DIR_NAME} */
omm.resources.video.DEF_DIR_NAME = '';

/** @define {string} Define para {@code omm.resources.video.DIR_PATH} */
omm.resources.video.DEF_DIR_PATH = '';

/** @define {string} Define para {@code omm.resources.video.LIVE_THUMBNAIL_FILENAME} */
omm.resources.video.DEF_LIVE_THUMBNAIL_FILENAME = '';

/** @define {string} Define para {@code omm.resources.video.LIVE_THUMBNAIL_PATH} */
omm.resources.video.DEF_LIVE_THUMBNAIL_PATH = '';

/**
 * Nombre base del directorio que guarda los recursos de Video
 * @const
 * @type {string}
 */
omm.resources.video.DIR_NAME =
  ((!COMPILED) && goog.global['OPENMULTIMEDIA_RESOURCES_VIDEO_DIRNAME']) ||
  omm.resources.video.DEF_DIR_NAME ||
  'video';

/**
 * Ruta del directorio que guarda los recursos de Video
 * @const
 * @type {string}
 */
omm.resources.video.DIR_PATH =
    ((!COMPILED) && goog.global['OPENMULTIMEDIA_RESOURCES_VIDEO_DIRPATH']) ||
    omm.resources.video.DEF_DIR_PATH ||
    omm.resources.DIR_PATH + '/' + omm.resources.video.DIR_NAME;

/**
 * Nombre base del archivo usado como Miniatura de la señal en Vivo
 * @const
 * @type {string}
 */
omm.resources.video.LIVE_THUMBNAIL_FILENAME =
  ((!COMPILED) && goog.global['OPENMULTIMEDIA_RESOURCES_VIDEO_LIVETHUMBNAILFILENAME']) ||
  omm.resources.video.DEF_LIVE_THUMBNAIL_FILENAME ||
  'live-thumbnail.png';

/**
 * Ruta del archivo usado como Miniatura de la señal en Vivo
 * @const
 * @type {string}
 */
omm.resources.video.LIVE_THUMBNAIL_PATH =
  ((!COMPILED) && goog.global['OPENMULTIMEDIA_RESOURCES_VIDEO_LIVETHUMBNAILPATH']) ||
  omm.resources.video.DEF_LIVE_THUMBNAIL_PATH ||
  (omm.resources.video.DIR_PATH + '/' + omm.resources.video.LIVE_THUMBNAIL_FILENAME);