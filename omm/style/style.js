goog.provide('omm.style');

omm.style.getPixelStyleNumberValue_ = function(val) {
  if (goog.isNumber(val)) {
    return val;
  }

  var matches = /(\d+\.)?(\d+)px/.exec(val);
    if (matches) {

    if (matches[1]) {
      return parseFloat(matches[1] + matches[2]);
    }

    return parseInt(matches[2], 10);
  }

  return null;
};
