goog.provide('omm.ui.BaseMap');
goog.provide('omm.ui.BaseMap.MapTypeOptions');
goog.provide('omm.ui.BaseMap.MapTypeType');

goog.require('goog.events');
goog.require('omm.externals.google.maps');
goog.require('goog.object');
goog.require('goog.ui.Component');
//goog.require('omm.ui.BaseMapConfiguration');
goog.require('omm.debug');
goog.require('goog.json');
goog.require('goog.style');
goog.require('omm.externals.ExternalsManager');
goog.require('omm.ui.DelayableComponent');

/**
 * @constructor
 * @extends {omm.ui.DelayableComponent}
 */
omm.ui.BaseMap = function(opt_options, opt_domHelper) {
  if ( goog.DEBUG ) this.logger_ =
    omm.debug.getLogger('omm.ui.BaseMap');

  var options = opt_options || {};

  this.mapOptions_ = options.mapOptions || { 'disableDefaultUI': true };

  this.mapTypeId_ = options.mapTypeId || false;

  this.mapTypes_ = {};

  if ( this.mapTypeId_ ) this.mapTypes_[ this.mapTypeId_] = 1;

  this.controls_ = [];

  this.zoom_ = options.zoom || 3;

  this.center_ = options.center ? options.center : null;

  this.width_ = options.width || '100%';

  this.height_ = options.height || '100%';

  goog.base(this, opt_domHelper);
};
goog.inherits(omm.ui.BaseMap, omm.ui.DelayableComponent);

omm.ui.BaseMap.prototype.isReady = function() {
  return (this.isInDocument() && this.isComponentInstantiated());
};

omm.ui.BaseMap.prototype.getWidth = function() {
  return this.width_ || '100%';
};

omm.ui.BaseMap.prototype.getHeight = function() {
  return this.height_ || '100%';
};

omm.ui.BaseMap.prototype.normalizeCoords_ = function(coords,
                                                                        zoom) {
  var y = coords.y, x = coords.x, tileRange;

  // tile range in one direction range is dependent on zoom level
  // 0 = 1 tile, 1 = 2 tiles, 2 = 4 tiles, 3 = 8 tiles, etc
  tileRange = (1 << zoom);

  // don't repeat across y-axis (vertically)
  if (y < 0 || y >= tileRange) {
    return null;
  }

  // repeat across x-axis
  if (x < 0 || x >= tileRange) {
    x = (x % tileRange + tileRange) % tileRange;
  }

  return { x: x, y: y };
};

omm.ui.BaseMap.prototype.makeTileUrl_ = function(baseUrl,
                                                                    coords,
                                                                    zoom) {
  var normalizedCoords =
      omm.ui.BaseMap.prototype.normalizeCoords_(coords, zoom);

  if (!normalizedCoords) {
    return null;
  }

  return baseUrl
      .replace('{zoom}', zoom)
      .replace('{x}', normalizedCoords.x)
      .replace('{y}', normalizedCoords.y);
};

// -- omm.ui.BaseMap

/**
 * Agrega un "tipo de mapa" al mapa
 * @param {omm.ui.BaseMap.MapTypeOptions} mapTypeOptions
 *     El objeto que define un tipo de mapa..
 * @param {bool=} opt_isDefault Indica si el tipo de mapa es el tipo de mapa por
 *     defecto.
 * @return {string} El id del tipo de mapa agregado.
 */
omm.ui.BaseMap.prototype.addMapType = function(mapTypeOptions,
                                                             opt_isDefault) {

  var MapTypeType = omm.ui.BaseMap.MapTypeType;

  this.mapTypes_[mapTypeOptions.id] = mapTypeOptions;

  if (this.isReady()) {
    this.addMapTypeInternal_(mapTypeOptions);
    this.updateMapTypeControl_();
  }

  if (opt_isDefault) {
    this.setMapType(mapTypeOptions.id);
  }

  return mapTypeOptions.id;
};

omm.ui.BaseMap.prototype.addMapTypeInternal_ = function(mapTypeOptions) {
  var MapTypeType = omm.ui.BaseMap.MapTypeType;

  if (mapTypeOptions.type == MapTypeType.IMAGE) {
    var mapType = new google.maps.ImageMapType({
      'name': mapTypeOptions.name,
      'alt': mapTypeOptions.alt || mapTypeOptions.name,
      'getTileUrl': goog.bind(this.makeTileUrl_, this, mapTypeOptions.tileUrl),
      'tileSize': new google.maps.Size(mapTypeOptions.tileSize.width, mapTypeOptions.tileSize.height),
      'maxZoom': mapTypeOptions.maxZoom,
      'minZoom': mapTypeOptions.minZoom || 0
    });

    this.map_.mapTypes.set(mapTypeOptions.id, mapType);
  }
};

omm.ui.BaseMap.prototype.getMapTypeIds = function() {
  var newMapTypeIds = [];

  for (var id in this.mapTypes_) {
    newMapTypeIds.push(id);
  }

  return newMapTypeIds;
};

omm.ui.BaseMap.prototype.updateMapTypeControl_ = function() {
  var newMapOptions = {};

  if (this.mapOptions_['mapTypeControlOptions']) {
    goog.object.extend(newMapOptions, this.mapOptions_['mapTypeControlOptions']);
  }

  goog.object.extend(newMapOptions, { 'mapTypeIds': this.getMapTypeIds() });

  goog.DEBUG && this.logger_.config( goog.json.serialize(newMapOptions) );

  this.map_.setOptions({'mapTypeControlOptions': newMapOptions});
};

omm.ui.BaseMap.prototype.setMapType = function(mapTypeId) {
  if (!(mapTypeId in this.mapTypes_))
    throw new Error('El tipo de mapa no es válido');

  this.mapTypeId_ = mapTypeId;

  if (this.isReady()) {
    this.map_.setMapTypeId(mapTypeId);
  } else {
    if ( this.mapTypes_[mapTypeId] ) {
      var customMapType = this.mapTypes_[mapTypeId];
      if ( customMapType.center ) {
        this.center_ = customMapType.center;
      }
    }
  }
};

omm.ui.BaseMap.prototype.addControl = function(control) {
    this.controls_.push(control);

    if ( this.isReady() ) {
        this.map_.controls[control.getPosition()].push(control.getDom());
    }
};

// -- goog.ui.Component
omm.ui.BaseMap.prototype.createDom = function() {
  this.setElementInternal(goog.dom.createDom('div'));
};

omm.ui.BaseMap.prototype.center = function(centerCoords) {
    if ( this.map_ ) {
        this.map_.setCenter(omm.externals.google.maps.makeLatLng(centerCoords));
    }
};

omm.ui.BaseMap.prototype.getMap = function() {
    return this.map_;
};

omm.ui.BaseMap.prototype.addPolygon = function(polyOptions) {
  var poly = omm.externals.google.maps.makePolygon(polyOptions);

  if ( this.isInDocument() )
    poly.setMap(this.map_);

  return poly;
};

/** @override */
omm.ui.BaseMap.prototype.getExternalsManager = function()  {
  return omm.ui.BaseMap.getExternalsManager();
};

omm.ui.BaseMap.prototype.enterDocument = function() {
  goog.base(this, 'enterDocument');

  var self = this;

  goog.events.listen(this.getContentElement(), 'mousemove', function(event) {
      self.dispatchEvent(event);
  });

  goog.style.setSize(this.getContentElement(), this.getWidth(), this.getHeight());

  goog.dom.classes.add(this.getContentElement(), goog.getCssName('openmultimedia-componentes-basemap-contenedor'));
};

omm.ui.BaseMap.prototype.enterComponent = function() {
  goog.base(this, 'enterComponent');

  // Creamos el mapa vacío
  this.map_ = new google.maps.Map(this.getContentElement());

  if ( this.mapTypes_ ) {
    for (var id in this.mapTypes_) {
      this.addMapTypeInternal_(this.mapTypes_[id]);
    }
  }

  if ( this.controls_.length > 0 ) {
    var currentControl;

    for ( var i = 0; i < this.controls_.length; i += 1 ) {
        currentControl = this.controls_[i];
        this.map_.controls[currentControl.getPosition()].push(currentControl.getDom());
    }
  }

  var options = goog.object.clone(this.mapOptions_);

  goog.object.extend(options, {
    'mapTypeId': this.mapTypeId_,
    'center': new google.maps.LatLng(this.center_.lat, this.center_.lng),
    'zoom': this.zoom_
  });

  if ( goog.DEBUG ) this.logger_.config('Aplicando configuración a mapa: ' + goog.json.serialize(options));

  this.map_.setOptions(options);

  this.updateMapTypeControl_();

  google.maps.event.addListener(this.map_, 'maptypeid_changed', function() {

  });

  var self = this;

  google.maps.event.addListener(this.map_, 'bounds_changed', function() {
    self.dispatchEvent('bounds_changed');
    self.onBoundsChanged_();
  });

  goog.events.dispatchEvent(this, 'ready');
}

omm.ui.BaseMap.prototype.onBoundsChanged_ = function() {
  var maptype = this.mapTypes_[this.map_.getMapTypeId()];

  if (maptype && maptype.keep_bounds) {
    var bounds = this.createLatLngBounds(maptype.keep_bounds);
    if (!this.getMap().getBounds().intersects(bounds))
      if (maptype.center)
        this.getMap().panTo(this.createLatLng(maptype.center));
      else
        this.getMap().panToBounds(bounds);
  }
};

omm.ui.BaseMap.prototype.createLatLng = function(descriptor) {
  return new google.maps.LatLng(descriptor.lat, descriptor.lng);
};

omm.ui.BaseMap.prototype.createLatLngBounds = function(descriptor) {
  return new google.maps.LatLngBounds(
    this.createLatLng(descriptor.sw),
    this.createLatLng(descriptor.ne)
  );
};

omm.ui.BaseMap.prototype.parseLatLng = function(latLng) {
  return omm.ui.BaseMap.parseLatLng(latLng);
};

/** @typedef {{id: string, type: omm.ui.BaseMap.MapTypeType}} */
omm.ui.BaseMap.MapTypeOptions;

/**
 * Tipos de "Tipo de Mapa"
 * @enum {number}
 */
omm.ui.BaseMap.MapTypeType = {
  /** Tipo de mapa estándar de Google Maps */
  BUILTIN: 0,
  /** Tipo de mapa de Imagen, para Tiles Personalizadas */
  IMAGE: 1
};

omm.ui.BaseMap.getExternalsManager = function() {
  if(!omm.ui.BaseMap.externalsManager_) {
    omm.ui.BaseMap.externalsManager_ =
        new omm.externals.ExternalsManager();

    omm.ui.BaseMap.externalsManager_.include(
      omm.externals.google.maps.registerExternal()
    );
  };

  return omm.ui.BaseMap.externalsManager_;
};

omm.ui.BaseMap.parseLatLng = function(latLng) {
  var pieces = latLng.split(',');
  return omm.externals.google.maps.makeLatLng({lat: pieces[0], lng: pieces[1]});
}
