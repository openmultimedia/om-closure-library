/**
 * @fileoverview Este archivo define la clase base para componentes de
 * instanciación retardada (Delayed Instantiation) utilizados principalmente
 * cuando se tiene un componente wrapper que require de una biblioteca externa
 * (como Google Maps) para poder hacer su instalación.
 * Debido a que la carga de la biblioteca externa require que la instanciación
 * se realize en dos pasos (la instanciación inmediata del wrapper, y la
 * instanciación del objeto externo una vez se carga su biblioteca), la misma
 * base del ccomponente puede ser utilizada para construir componentes pesados
 * cuya instanciación pueda ser retrasada y controlada por otro componente.
 * @author kg.designer@openmultimedia.biz (Alan Reyes)
 */
goog.provide('omm.ui.DelayableComponent');

goog.require('goog.debug.Logger');
goog.require('goog.ui.Component');
goog.require('omm.debug');
goog.require('omm.externals.ExternalsManager');

/**
 * Clase base para un componente que puede retrasar su instanciación debido al
 * requerimiento de externos o hasta una señal explícita.
 * @param {goog.dom.DomHelper=} opt_domHelper El DomHelper a usar con este
 *     componente.
 * @constructor
 * @extends {goog.ui.Component}
 */
omm.ui.DelayableComponent = function(opt_domHelper) {
  this.componentDelayed_ = false;
  this.componentInstanceKey_ = 0;
  this.componentInstantiated_ = false;
  this.componentInstantiating_ = false;

  goog.base(this, opt_domHelper);
};
goog.inherits(omm.ui.DelayableComponent, goog.ui.Component);


/**
 * Devuelve un Logger para la instancia de esta clase
 * @return {goog.debug.Logger} El Logger para esta clase.
 * @protected
 */
omm.ui.DelayableComponent.prototype.getLogger = function() {
  return omm.debug.createLogger(
      'omm.ui.DelayableComponent', this);
};


/**
 * Activa/desactiva la instanciación retardada de esta instancia
 * @param {boolean} status El nuevo estado de la instanciacion retardada.
 */
omm.ui.DelayableComponent.prototype.setComponentDelayed =
    function(status) {
  if (!this.componentInstantiating_ && !this.componentInstantiated_) {
    this.componentDelayed_ = !!status;
  } else {
    throw new Error('El componente ya ha sido instanciado');
  }
};


/**
 * Indica si el componente tiene activada la instanciación retardada
 * @return {boolean} Indica si esta activa la instanciación retardada.
 */
omm.ui.DelayableComponent.prototype.isComponentDelayed =
    function() {
  return this.componentDelayed_;
};


/**
 * Llave de sincronización del componente. Ayuda a prevenir que una llamada
 * asíncrona de instanciación se ejecute después de que el componente se ha
 * dehabilitado y rehabilitado en el DOM.
 * @return {number} Llave actual de sincronización.
 */
omm.ui.DelayableComponent.prototype.getComponentInstanceKey =
    function() {
  return this.componentInstanceKey_;
};


/**
 * Devuelve cuando el componente actual se encuentra en fase de instanciación.
 * Esta fase abarca el tiempo de ejecución de la función de instanciación {@code
 * enterComponent} pero también el tiempo de espera si se están utilizando
 * externos.
 * @return {boolean} Cuando este componente esta en fase de instanciación.
 */
omm.ui.DelayableComponent.prototype.
    isComponentInstantiating = function() {
  return this.componentInstantiating_;
};


/**
 * Devuelve cuando el componente actual se encuentra instanciado.
 * @return {boolean} Cuando este componente esta instanciado.
 */
omm.ui.DelayableComponent.prototype.isComponentInstantiated =
    function() {
  return this.componentInstantiated_;
};


/**
 * Función de ayuda para ejecutar la instanciación del componente hasta que se
 * esté seguro que los externos de este componente estén completamente cargados
 * y listos.
 * @param {function()} callback La función a llamar cuando se terminen de cargar
 *     los externos.
 * @private
 */
omm.ui.DelayableComponent.prototype.ensureExternalsReady_ =
    function(callback) {
  var externalsManager = this.getExternalsManager();
  if (!externalsManager || externalsManager.isLoaded()) {
    callback();
  } else {
    goog.events.listen(externalsManager, 'load', callback);

    if (!externalsManager.isLoading()) {
      externalsManager.load();
    }
  }
};


/**
 * Función helper para verificar si la llamada asíncrona para instanciar el
 * componente aún es válida (por ejemplo, tras cargar externos). Si es válida,
 * ejecuta la llamada a enterDocument
 * @param {number} componentInstanceKey La llave de instancia original con que
 *     se invocó la instanciación del componente.
 * @private
 */
omm.ui.DelayableComponent.prototype.onEnterComponent_ =
    function(componentInstanceKey) {
  if (this.componentInstanceKey == componentInstanceKey) {
    this.enterComponent();
  } else {
    if (omm.debug.LOGGING_ENABLED) {
      this.getLogger().warn('No se instanció con Key ' + componentInstanceKey +
                            ' porque el Key ya cambió');
    }
  }
};


/**
 * Invoca explícitamente la instanciación del componente. La llamada a esta
 * función es requerida cuando se {@code setComponentDelayed} se ha activado
 */
omm.ui.DelayableComponent.prototype.instantiateNow =
    function() {
  if (!this.componentInstantiated_ && !this.componentInstantiating_) {
    this.componentInstantiating_ = true;
    this.ensureExternalsReady_(goog.bind(this.onEnterComponent_, this,
                                         this.instanceKey_));
  } else {
    if (this.componentInstantiated_) {
      throw new Error('Component already instantiated');
    }

    if (this.componentInstantiating_) {
      throw new Error('Component is instantiating');
    }
  }
};


/**
 * El método que instancia las características retardadas del componente. Este
 * método debe ser sobrecargado por las clases hijas, y debe ser llamado por
 * ellas al final de su instanciación.
 */
omm.ui.DelayableComponent.prototype.enterComponent =
    function() {
  this.componentInstantiating_ = false;
  this.componentInstantiated_ = true;
};


/**
 * El método que desinstala las características retardadas del componente. Este
 * método debe ser sobrecargado por las clases hijas, y debe ser llamado por
 * ellas al final de su lógica de salida.
 */
omm.ui.DelayableComponent.prototype.exitComponent =
    function() {
  this.componentInstantiated_ = false;
};


/**
 * Este método devuelve el ExternalsManager que usará este componente.
 * Se recomienda que el ExternalsManager se almacene en una propiedad estática
 * de la clase hija. Si esta funcion no se sobrecarga o devuelve null, se
 * considera que el componente no utiliza componentes externos y la
 * instanciación dejará de ser asíncrona.
 * @return {?omm.externals.ExternalsManager} El manejador de
 *     externos a utilizar.
 * @protected
 */
omm.ui.DelayableComponent.prototype.getExternalsManager =
    function() {
  //override
  return null;
};


/**
 * @override
 */
omm.ui.DelayableComponent.prototype.enterDocument =
    function() {
  if (!this.isComponentDelayed()) {
    this.instantiateNow();
  }
  goog.base(this, 'enterDocument');
};


/**
 * @override
 */
omm.ui.DelayableComponent.prototype.exitDocument =
    function() {
  if (this.isInstantiated()) {
    this.exitComponent();
  }

  this.instanceKey_ += 1;

  goog.base(this, 'exitDocument');
};
