goog.provide("omm.ui.FeedList")

goog.require("omm.ui.FeedListTemplates")
goog.require("omm.ui.FeedListOptions")
goog.require("omm.ui.FeedListLocalization")
goog.require('omm.ui.FeedListItem')

goog.require("omm.externals.google.Loader.feeds")
goog.require("omm.configuration")
goog.require("goog.ui.Component")

omm.externals.google.Loader.feeds.requireLib("1")

omm.ui.FeedList = function(medio, opt_options, opt_localization, opt_domHelper) {
    this.medio_ = medio;

    this.options_ = omm.configuration.complete(opt_options, omm.ui.FeedListOptions)

    this.localization_ = omm.localization.extend(omm.ui.FeedListLocalization, opt_localization)

    goog.base(this, opt_domHelper);
};

goog.inherits(omm.ui.FeedList, goog.ui.Component);

omm.ui.FeedList.prototype.createDom = function () {
    var lang = this.medio_.getLanguageCode()

    this.element_ = soy.renderAsFragment(omm.ui.FeedListTemplates.container, {localization: this.localization_[lang]});
    this.titleElement_ = goog.dom.getElementByClass(goog.getCssName("openmultimedia-componentes-feedlist-title"), this.element_);
    this.contentElement_ = goog.dom.getElementByClass(goog.getCssName("openmultimedia-componentes-feedlist-content"), this.element_);
}

omm.ui.FeedList.prototype.getContentElement = function() {
    return this.contentElement_;
}

omm.ui.FeedList.prototype.enterDocument = function () {
    var feed_url = this.medio_.getFeedUrl( this.options_.site, this.options_.type )

    this.feed_ = new google.feeds.Feed(feed_url)

    this.feed_.setResultFormat(google.feeds.Feed.MIXED_FORMAT)

    this.feed_.setNumEntries(10);

    function onFeedLoad(result) {
        if ( ! result['error'] ) {
            this.displayFeeds(result['feed'])
        }
    }

    this.feed_.load(goog.bind(onFeedLoad, this))

    goog.base(this, 'enterDocument')
}

omm.ui.FeedList.prototype.displayFeeds = function (feed) {
    this.removeChildren(true);

    /** @type {omm.ui.FeedListItem}*/
    var feedItem;

    var entries = /** @type {Array} */ feed['entries'];

    for ( var i = 0; i < entries.length; i += 1 ) {
        feedItem = new omm.ui.FeedListItem(entries[i], this.options_, this.getDomHelper());
        this.addChild(feedItem, true);
        //goog.events.listen(feedItem, goog.events.EventType.CLICK, this.onItemClick_);
    }
}
