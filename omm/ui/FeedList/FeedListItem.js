goog.provide("omm.ui.FeedListItem");

goog.require("omm.ui.FeedListTemplates");
goog.require("omm.utils.TimeFormatter");
goog.require("goog.ui.Component");
goog.require("goog.events.EventType");

omm.ui.FeedListItem = function (feedItem, feedListOptions, opt_domHelper) {
    this.data_ = feedItem;
    this.feedListOptions_ = feedListOptions;

    var templateData = {
        titulo: feedItem['title']
        ,url: feedItem['xmlNode'].getElementsByTagName('link').item(0).getAttribute('href')
    }

    if ( feedListOptions['show_content'] ) {
        if ( feedListOptions['show_content'] == 'snippet' ) {
            templateData.contenido = feedItem['contentSnippet']
        } else {
            templateData.contenido = feedItem['content']
        }
    } else {
        templateData.contenido = false
    }

    this.templateData_ = templateData

    //this.onClick_ = goog.bind(this.onClick_, this);

    goog.base(this, opt_domHelper)
}

goog.inherits(omm.ui.FeedListItem, goog.ui.Component);

omm.ui.FeedListItem.prototype.getClipData = function() {
    return this.data_
}

omm.ui.FeedListItem.prototype.createDom = function() {
    this.element_ = soy.renderAsFragment(omm.ui.FeedListTemplates.item, this.templateData_)
}

omm.ui.FeedListItem.prototype.enterDocument = function () {
    //goog.events.listen(this.element_, goog.events.EventType.CLICK, this.onClick_);

    goog.base(this, "enterDocument")
}

omm.ui.FeedListItem.prototype.exitDocument = function () {
    //goog.events.unlisten(this.element_, goog.events.EventType.CLICK, this.onClick_);

    goog.base(this, "exitDocument")
}

omm.ui.FeedListItem.prototype.onClick_ = function (event) {
    this.dispatchEvent(new goog.events.Event(goog.events.EventType.CLICK))
}
