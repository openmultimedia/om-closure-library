goog.provide("omm.ui.FeedListOptions");
goog.provide("omm.ui.FeedListLocalization");

omm.ui.FeedListOptions = {
    site: 'home'
    ,type: 'atom' // atom, rss2, etc. Los soportados por el Medio
    ,show_content: true // true || 'complete'; snippet; false
}

omm.ui.FeedListLocalization = {
    'es': {
        titulo: "Titulo"
    }
}
