goog.provide('omm.ui.JwplayerBase');

goog.require('goog.ui.Component');
goog.require('omm.debug');
goog.require('omm.externals.jwplayer');

omm.ui.JwplayerBase = function(opt_options, opt_domHelper) {
  this.logger_ =
    omm.debug.getLogger('omm.ui.JwplayerBase', this);

  var options = opt_options || {};

  this.logger_.config('Instantiating with configuration: ' +
    omm.debug.formatJson(options));

  this.playerId_ = this.makeId('jwplayer-uid');

  this.syncId_ = 0;

  this.modes_ = options.modes ||
      [omm.externals.jwplayer.RenderingMode.Flash,
       omm.externals.jwplayer.RenderingMode.Html5,
       omm.externals.jwplayer.RenderingMode.Download];

  this.playlist_ = options.playlist || [];

  this.autoplayMode_ = options.autoplay ||
      omm.ui.JwplayerBase.AutoplayMode.Disabled;

  this.width_ = options.width;

  this.height_ = options.height;

  this.skin_ = options.skin;

  this.controlbar_position_ = options.controlbar_position;

  this.plugins_ = options.plugins;

  this.playerReady_ = false;

  this.icons_ = options.icons;

  this.logo_ = options.logo;

  this.instantiated_ = false;

  goog.base(this, opt_domHelper);
};
goog.inherits(omm.ui.JwplayerBase, goog.ui.Component);

omm.ui.JwplayerBase.prototype.getSize = function() {
  return new goog.math.Size(this.width_, this.height_);
};

omm.ui.JwplayerBase.prototype.resize = function(width, height) {
  if ((width == this.width_) && (height == this.height_)) return;

  this.width_ = width;
  this.height_ = height;

  goog.style.setSize(this.getElement(), width, height);

  var style = this.getPlayerStyle();

  style.setContainerSize(width, height);

  var newSize = style.getPlayerSize();

  if (this.isPlayerReady())  {
    this.player_.resize(newSize.width, newSize.height);
  }
};

omm.ui.JwplayerBase.prototype.autoplayEnableOnce = function() {
  var AutoplayMode = omm.ui.JwplayerBase.AutoplayMode;
  this.logger_.info('Enabling Autoplay Once');

  if (this.autoplayMode_ == AutoplayMode.Disabled)
    this.autoplayMode_ = AutoplayMode.EnabledOnce;
  else if (this.autoplayMode_ == AutoplayMode.DisabledOnce)
    this.autoplayMode_ = AutoplayMode.Enabled;
}

omm.ui.JwplayerBase.prototype.autoplayDisableOnce = function() {
  var AutoplayMode = omm.ui.JwplayerBase.AutoplayMode;
  this.logger_.info('Disabling Autoplay Once');

  if (this.autoplayMode_ == AutoplayMode.Enabled)
    this.autoplayMode_ = AutoplayMode.DisabledOnce;
  else if (this.autoplayMode_ == AutoplayMode.EnabledOnce)
    this.autoplayMode_ = AutoplayMode.Disabled;
};

omm.ui.JwplayerBase.prototype.isPlayerReady = function() {
  return this.isInDocument() && this.playerReady_;
};

omm.ui.JwplayerBase.prototype.isInstantiated = function() {
  return this.instantiated_;
};

omm.ui.JwplayerBase.prototype.getInternalPlayer = function() {
    return this.player_;
}

omm.ui.JwplayerBase.prototype.getPlayerId = function() {
  return this.playerId_;
}

omm.ui.JwplayerBase.prototype.setPlayerStyle = function(style) {
  this.playerStyle_ = style;
  this.playerStyle_.setPlayer(this);
}

omm.ui.JwplayerBase.prototype.getPlayerStyle = function(style) {
  return this.playerStyle_;
}

omm.ui.JwplayerBase.prototype.play = function() {
  this.logger_.info('Force play');

  if (this.isPlayerReady()) {
    this.logger_.info('Player is ready, sending command');
    this.player_.play(true);
  }
  else {
    this.logger_.info('Player is not ready');
    this.autoplayEnableOnce();
  }
};

omm.ui.JwplayerBase.prototype.pause = function() {
  var AutoplayMode = omm.ui.JwplayerBase.AutoplayMode;

  if (this.isPlayerReady()) {
    this.logger_.info('Player is ready, sending (play) command');
    this.player_.pause(true);
  }
  else {
    this.logger_.info('Player is not ready');
    this.autoplayDisableOnce();
  }
};

omm.ui.JwplayerBase.prototype.toggle = function() {
  var AutoplayMode = omm.ui.JwplayerBase.AutoplayMode;

  if (this.isPlayerReady()) {
    this.logger_.info('Player is ready, sending play (toggle) command');
    this.player_.play();
  }
  else {
    this.logger_.info('Player is not ready');

    if ((this.autoplayMode_ == AutoplayMode.Enabled) || (this.autoplayMode_ == AutoplayMode.EnabledOnce)) {
      this.autoplayDisableOnce();
    } else {
      this.autoplayEnableOnce();
    }
  }
};

omm.ui.JwplayerBase.prototype.stop = function() {
  if (this.isPlayerReady()) {
    this.logger_.info('Player is ready, sending (stop) command');
    this.player_.stop();
  }
  else {
    this.logger_.info('Player is not ready');
    this.autoplayDisableOnce();
  }
}

omm.ui.JwplayerBase.prototype.getCurrentItem = function() {
  if(this.player_)
    return this.player_.getPlaylistItem();
  else
    return null;
};

omm.ui.JwplayerBase.prototype.loadItem = function(
    item, opt_autoplay) {

  this.logger_.info('Loading item: '  +
    omm.debug.formatJson(item));

  this.loadPlaylist([item], opt_autoplay);
};

omm.ui.JwplayerBase.prototype.loadPlaylist = function(
    playlist, opt_autoplay) {

  this.logger_.info('Loading playlist (Autoplay: ' + opt_autoplay +'):'  +
      omm.debug.formatJson(playlist));

  var AutoplayMode = omm.ui.JwplayerBase.AutoplayMode;

  var playlistIsArray = goog.isArray(playlist);

  this.logger_.info('Registering playlist as current');

  this.playlist_ = playlist;

  if ( goog.isDef(opt_autoplay) ) {
    if (opt_autoplay)
      this.autoplayEnableOnce();
    else
      this.autoplayDisableOnce();
  }

  if (playlist && this.isPlayerReady()) {
    this.logger_.info('Player is ready in mode: ' +
                      this.player_.getRenderingMode());

    var finalPlaylist = null;

    if (! playlistIsArray) {
      var currentMode = this.player_.getRenderingMode();

      if (currentMode in playlist)
        finalPlaylist = playlist[currentMode];

    } else
      finalPlaylist = playlist;

    this.logger_.info('Processed playlist to load: ' +
                      omm.debug.formatJson(finalPlaylist));

    this.logger_.info('Sending command');

    this.player_.load(finalPlaylist);
  } else {
    this.logger_.info('Player is not ready, enqueuing ');
  }
};


/** @override */
omm.ui.JwplayerBase.prototype.createDom = function() {
  var styleText = goog.style.toStyleAttribute({
    'width': this.width_ ? this.width_ + 'px' : '100%',
    'height': this.height_ ? this.height_ + 'px' : '100%',
    'display': 'block',
    'overflow': 'hidden'
  });

  var element = goog.dom.createDom('div', { 'id': this.playerId_, 'style': styleText });

  this.setElementInternal(element);
};

omm.ui.JwplayerBase.prototype.getContentElement = function() {
  var style = this.getPlayerStyle();
  return style.getContentElement();
}

omm.ui.JwplayerBase.prototype.enterDocument = function() {
  this.logger_.info('Entering document');

  if ( ! goog.isDef(this.width_) || ! goog.isDef(this.height_) ) {
    var size = goog.style.getSize(this.getElement());

    if (!goog.isDef(this.width_)) this.width_ = size.width;
    if (!goog.isDef(this.height_)) this.height_ = size.height;
  }

  var playerStyle = this.getPlayerStyle();

  if (!playerStyle) {
    playerStyle = new omm.ui.JwplayerDefaultStyle();
    this.setPlayerStyle(playerStyle);
  }

  playerStyle.setContainerSize(this.width_, this.height_);

  playerStyle.render(this.getElement());

  var jwOptions = {};

  var modes = [];

  for (var i = 0; i < this.modes_.length; i += 1) {
    var currentMode = this.modes_[i];

    modes[i] = {'type': currentMode};

    if (currentMode == omm.externals.jwplayer.RenderingMode.Flash) {
      modes[i]['src'] = omm.externals.jwplayer.getSwfUrl();
    }
  }

  jwOptions['modes'] = modes;

  if ( this.skin_ )
    jwOptions['skin'] = this.skin_

  if ( this.controlbar_position_ )
    jwOptions['controlbar.position'] = this.controlbar_position_;

  if ( this.plugins_ ) {
    jwOptions['plugins'] = this.plugins_;
  }

  if (goog.isDef(this.icons_))
    jwOptions['icons'] = this.icons_;

  if (this.logo_)
    jwOptions['logo'] = this.logo_;

  goog.object.extend(jwOptions, this.playerStyle_.getPlayerOptions());

  this.logger_.config(
      'Installing jwplayer with options: ' +
      omm.debug.formatJson(jwOptions));

  var self = this;

  goog.base(this, 'enterDocument');

  playerStyle.playerEnterDocument();

  this.jwOptions_ = jwOptions;

  if ( ! playerStyle.getDelayPlayer() ) {
    this.instantiate();
  }
};

omm.ui.JwplayerBase.prototype.getSyncId = function() {
  return this.syncId_;
};

omm.ui.JwplayerBase.prototype.instantiate = function() {
  if ( ! this.instantiated_ ) {
    this.instantiated_ = true;
    omm.ui.JwplayerBase.ensureExternalsReady(goog.bind(this.instantiatePlayer_, this, this.syncId_, this.jwOptions_));
  }
}

omm.ui.JwplayerBase.prototype.instantiatePlayer_ = function(syncId, jwOptions) {
  // If syncId has changed, Component has been disposed and reenabled so it must not be installed anymore
  if (syncId !== this.syncId_ || !this.isInDocument()) return;

  // Aqui inserta el tamaño del player segpun el estilo por el caso de los delayed player que pueden haber cambiado de tamaño, antes de instanciarse
  var size = this.playerStyle_.getPlayerSize();

  goog.object.extend(jwOptions, {'width': size.width, 'height': size.height});

  this.logger_.config('Instantiating player with: ' + goog.json.serialize(jwOptions));

  this.player_ = jwplayer(this.getContentElement()).setup(jwOptions);

  function onJwplayerEvent(jwplayerBase, type, event) {
    jwplayerBase.dispatchEvent(new omm.ui.JwplayerBase.JwplayerEvent(type, event, this));
  }

  /** @this {jwplayer.Player} */
  function onJwplayerReady(jwplayerBase, event) {
    jwplayerBase.logger_.info('jwPlayer: OnReady');

    if (syncId !== jwplayerBase.syncId_ || !jwplayerBase.isInDocument()) {
      this.remove();
      return;
    };

    jwplayerBase.playerReady_ = true;

    var pendingVideoList = jwplayerBase.playlist_;

    this.stop();

    if ( pendingVideoList ) {
      jwplayerBase.logger_.info('Loading pending playlist');
      jwplayerBase.loadPlaylist(pendingVideoList);
    }
  };

  /** @this {jwplayer.Player} */
  function onJwplayerPlaylist(jwplayerBase, event) {
    jwplayerBase.logger_.info('jwPlayer: New playlist loaded');

    var AutoplayMode = omm.ui.JwplayerBase.AutoplayMode;

    var autoplay = jwplayerBase.autoplayMode_;

    if (autoplay == AutoplayMode.Enabled || autoplay == AutoplayMode.EnabledOnce) {
      jwplayerBase.play();

      if (autoplay == AutoplayMode.EnabledOnce) {
        jwplayerBase.autoplayMode_ = AutoplayMode.Disabled;
      }
    } else if ( autoplay == AutoplayMode.DisabledOnce ) {
      jwplayerBase.autoplayMode_ = AutoplayMode.Enabled;
    }
  };

  this.player_.onReady(goog.partial(onJwplayerReady, this));

  this.player_.onPlaylist(goog.partial(onJwplayerPlaylist, this));

  var self = this;

  this.player_.onError(function(e){
    self.logger_.error(e);
  });

  this.player_.
    onBufferChange(goog.partial(onJwplayerEvent, this,
      omm.ui.JwplayerBase.EventType.BUFFER_CHANGE)).
    onBufferFull(goog.partial(onJwplayerEvent, this,
      omm.ui.JwplayerBase.EventType.BUFFER_FULL)).
    onError(goog.partial(onJwplayerEvent, this,
      omm.ui.JwplayerBase.EventType.ERROR)).
    onFullscreen(goog.partial(onJwplayerEvent, this,
      omm.ui.JwplayerBase.EventType.FULLSCREEN)).
    onMeta(goog.partial(onJwplayerEvent, this,
      omm.ui.JwplayerBase.EventType.META)).
    onMute(goog.partial(onJwplayerEvent, this,
      omm.ui.JwplayerBase.EventType.MUTE)).
    onPlaylist(goog.partial(onJwplayerEvent, this,
      omm.ui.JwplayerBase.EventType.PLAYLIST)).
    onPlaylistItem(goog.partial(onJwplayerEvent, this,
      omm.ui.JwplayerBase.EventType.PLAYLIST_ITEM)).
    onReady(goog.partial(onJwplayerEvent, this,
      omm.ui.JwplayerBase.EventType.READY)).
    onResize(goog.partial(onJwplayerEvent, this,
      omm.ui.JwplayerBase.EventType.RESIZE)).
    onBeforePlay(goog.partial(onJwplayerEvent, this,
      omm.ui.JwplayerBase.EventType.BEFORE_PLAY)).
    onPlay(goog.partial(onJwplayerEvent, this,
      omm.ui.JwplayerBase.EventType.PLAY)).
    onPause(goog.partial(onJwplayerEvent, this,
      omm.ui.JwplayerBase.EventType.PAUSE)).
    onBuffer(goog.partial(onJwplayerEvent, this,
      omm.ui.JwplayerBase.EventType.BUFFER)).
    onSeek(goog.partial(onJwplayerEvent, this,
      omm.ui.JwplayerBase.EventType.SEEK)).
    onIdle(goog.partial(onJwplayerEvent, this,
      omm.ui.JwplayerBase.EventType.IDLE)).
    onComplete(goog.partial(onJwplayerEvent, this,
      omm.ui.JwplayerBase.EventType.COMPLETE)).
    onTime(goog.partial(onJwplayerEvent, this,
      omm.ui.JwplayerBase.EventType.TIME)).
    onVolume(goog.partial(onJwplayerEvent, this,
      omm.ui.JwplayerBase.EventType.VOLUME));
}


/** @override */
omm.ui.JwplayerBase.prototype.exitDocument = function() {
   this.logger_.info('Exiting document');

   if (!this.isInDocument()) return;

  goog.base(this, 'exitDocument');

  if (this.player_ && this.playerReady_) {
    this.player_.remove();
  }

  this.player_ = null;
  this.playerReady_ = false;
  this.instantiated_ = false;

  goog.base(this, 'exitDocument');

  this.playerStyle_.exitDocument();

  this.syncId_ += 1;
};

omm.ui.JwplayerBase.prototype.disposeInternal = function() {
  goog.base(this, 'disposeInternal');

  this.logger_ = null;
  this.playerStyle_ = null;
};

omm.ui.JwplayerBase.prototype.dispose = function() {
  var playerStyle = this.playerStyle_;
  goog.base(this, 'dispose');

  if(playerStyle) {
    playerStyle.dispose();
  }
};

// Static

omm.ui.JwplayerBase.UID_PROPERTY_ = 'jwplayer-uid-' +
    Math.floor(Math.random() * 2147483648).toString(36);

omm.ui.JwplayerBase.AutoplayMode = {
  Disabled: 'disabled',
  DisabledOnce: 'disabled_once',
  Enabled: 'enabled',
  EnabledOnce: 'enabled_once'
};

omm.ui.JwplayerBase.JwplayerEvent = function(type, jwplayerEvent, target) {
  if ( jwplayerEvent ) {
    for (var k in jwplayerEvent) {
      if ( jwplayerEvent.hasOwnProperty(k) ) {
        this[k] = jwplayerEvent[k];
      }
    }
  }

  goog.base(this, type, target);
}

goog.inherits(omm.ui.JwplayerBase.JwplayerEvent,
  goog.events.Event);

omm.ui.JwplayerBase.EventType = {
  BUFFER_CHANGE: 'buffer_change',
  BUFFER_FULL: 'buffer_full',
  ERROR: 'error',
  FULLSCREEN: 'fullscreen',
  META: 'meta',
  MUTE: 'mute',
  PLAYLIST: 'playlist',
  PLAYLIST_ITEM: 'playlist_item',
  READY: 'ready',
  RESIZE: 'resize',
  BEFORE_PLAY: 'before_play',
  PLAY: 'play',
  PAUSE: 'pause',
  BUFFER: 'buffer',
  SEEK: 'seek',
  IDLE: 'idle',
  COMPLETE: 'complete',
  TIME: 'time',
  VOLUME: 'volume'
};

omm.ui.JwplayerBase.ensureExternalsReady = function(callback) {
  var JwplayerBase = omm.ui.JwplayerBase;

  if(!JwplayerBase.externalsManager_) {
    JwplayerBase.externalsManager_ = new omm.externals.ExternalsManager();
    JwplayerBase.externalsManager_.include(omm.externals.jwplayer.registerExternal());
  }

  if (JwplayerBase.externalsManager_.isLoaded())
    callback();
  else {
    goog.events.listen(JwplayerBase.externalsManager_, 'load', callback);

    if (!JwplayerBase.externalsManager_.isLoading()) {
      JwplayerBase.externalsManager_.load();
    }
  }
};
