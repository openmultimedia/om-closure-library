goog.provide('omm.ui.JwplayerDefaultStyle');

goog.require('omm.ui.JwplayerDefaultStyleTemplates');

goog.require('goog.ui.Component');

omm.ui.JwplayerDefaultStyle = function(opt_domHelper) {
  this.playerSize_;
  goog.base(this, opt_domHelper);
};

goog.inherits(omm.ui.JwplayerDefaultStyle, goog.ui.Component);

omm.ui.JwplayerDefaultStyle.prototype.setPlayer = function(player) {
  this.player_ = player;
};

omm.ui.JwplayerDefaultStyle.prototype.getPlayer = function() {
  return this.player_;
};

omm.ui.JwplayerDefaultStyle.prototype.getPlayerId = function() {
    return this.player_.getPlayerId() + '_player';
};

omm.ui.JwplayerDefaultStyle.prototype.setContainerSize = function(sizeOrWidth, height) {
  this.containerSize_ = sizeOrWidth instanceof goog.math.Size ? sizeOrWidth: new goog.math.Size(sizeOrWidth, height);
  if (this.isInDocument()) {
    this.adjustSize();
  }
};

omm.ui.JwplayerDefaultStyle.prototype.getContainerSize = function() {
  return this.containerSize_;
};

omm.ui.JwplayerDefaultStyle.prototype.adjustSize = function() {
  var element = this.getElement();
  this.setPlayerSize(this.containerSize_);

  if (this.isInDocument()) {
    goog.style.setSize(element, this.containerSize_);
  }
};

omm.ui.JwplayerDefaultStyle.prototype.setPlayerSize = function(sizeOrWidth, height) {
  this.playerSize_ = sizeOrWidth instanceof goog.math.Size ? sizeOrWidth: new goog.math.Size(sizeOrWidth, height);
};

omm.ui.JwplayerDefaultStyle.prototype.getPlayerSize = function() {
  return this.playerSize_;
};

omm.ui.JwplayerDefaultStyle.prototype.getPlayerOptions = function() {
  return {};
};

omm.ui.JwplayerDefaultStyle.prototype.getDelayPlayer = function() {
  return false;
};

omm.ui.JwplayerDefaultStyle.prototype.createDom = function() {
    var element = soy.renderAsFragment(
        omm.ui.JwplayerDefaultStyleTemplates.container,
        {
            player_id: this.getPlayerId(),
            size: this.getContainerSize()
        }
    );

    this.setElementInternal(element);
    this.contentElement_  = element;
};

omm.ui.JwplayerDefaultStyle.prototype.getContentElement = function() {
    return this.contentElement_;
};

omm.ui.JwplayerDefaultStyle.prototype.enterDocument = function() {
    goog.base(this, 'enterDocument');
};

omm.ui.JwplayerDefaultStyle.prototype.playerEnterDocument = function() {
  this.adjustSize();
}
