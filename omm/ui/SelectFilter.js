goog.provide('omm.ui.SelectFilter');

goog.require('goog.ui.Select');
goog.require('goog.ui.Option');
goog.require('goog.events.EventType');
goog.require('omm.localization');
goog.require('omm.utils');

omm.ui.SelectFilter = function (node, options) {
  this.node_ = node;

  this.setOptions(options);

  this.render();
};

omm.ui.SelectFilter.prototype.node_ = null;

omm.ui.SelectFilter.prototype.defaultItem_ = '';

omm.ui.SelectFilter.prototype.items_ = [];

omm.ui.SelectFilter.prototype.itemFilterMap_ = {};

omm.ui.SelectFilter.prototype.localization_ = {
  'es': {},
  'en': {},
  'pt': {}
};

omm.ui.SelectFilter.prototype.selectControl_ = null;

omm.ui.SelectFilter.prototype.setOptions = function (options) {
  goog.DEBUG && console.groupCollapsed('omm.ui.SelectFilter.setOptions');

  goog.DEBUG && console.log('Setting options', options);

  if ( ! options ) {
    return;
  }

  var newLocalization = omm.utils.retrieveSubOptions(options, 'localization');

  this.setMainOptions(options);

  if ( newLocalization ) {
    this.setLocalization(newLocalization);
  }

  goog.DEBUG && console.groupEnd();
};

omm.ui.SelectFilter.prototype.setMainOptions = function (options) {
  var newItems = options['items'];

  this.items_ = newItems;
  this.itemFilterMap_ = {};

  var currentItem;

  for (var i = 0; i < newItems.length; i += 1 ) {
    currentItem = newItems[i];
    goog.DEBUG && console.log('Adding item: ', currentItem);
    this.itemFilterMap_[ currentItem['slug'] ] = currentItem;
  }

  var fullOptions = omm.utils.completeOptions(this.options_, options);

  this.options_ = fullOptions;
};

omm.ui.SelectFilter.prototype.setLocalization = function (localization) {
  var newLocalization = omm.localization.extend(this.localization_, localization);

  this.localization_ = newLocalization;
}

omm.ui.SelectFilter.prototype.render = function () {
  goog.DEBUG && console.groupCollapsed('omm.ui.SelectFilter.render');

  goog.DEBUG && console.log( 'Setting items', this.items_ );

  this.selectControl_ = new goog.ui.Select();

  var currentItem;

  for ( var i = 0; i < this.items_.length; i += 1 ) {
    currentItem = this.items_[i];
    this.selectControl_.addItem( new goog.ui.Option( currentItem['slug'] ) );
  }

  goog.events.listen( this.selectControl_, goog.events.EventType.CHANGE, this.onSelectChange_ );

  goog.DEBUG && console.log( 'LeNodei', this.node_, this.selectControl_ );

  this.selectControl_.render(this.node_);

  goog.DEBUG && console.groupEnd();
};

omm.ui.SelectFilter.prototype.onSelectChange_ = function (event) {
  goog.DEBUG && console.log('SelectChange', arguments);
}