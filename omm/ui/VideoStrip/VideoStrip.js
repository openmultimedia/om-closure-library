goog.provide("omm.ui.VideoStrip")

goog.require("omm.ui.VideoStripTemplates")
goog.require("omm.ui.VideoStripItem")
goog.require("omm.ui.VideoStripOptions")
goog.require("omm.ui.VideoStripEvent");
goog.require("omm.ui.VideoStripEventType");
goog.require("goog.ui.Component");
goog.require("omm.api.ManejadorApi");
goog.require("goog.events");
goog.require("goog.events.EventType");
goog.require("goog.object");
goog.require("omm.api");

/**
 * @constructor
 */
omm.ui.VideoStrip = function(medio, opt_options, opt_domHelper) {
    this.medio_ = medio;

    this.options_ = omm.configuration.complete(opt_options, omm.ui.VideoStripOptions);

    this.controls_ = this.options_.controls ? this.options_.controls : null;

    this.manejadorApi_ = new omm.api.ManejadorApi(medio, this.options_.lang);

    // Pre-binded event helpers
    this.onItemClick_ = goog.bind(this.onItemClick_, this);

    goog.base(this, opt_domHelper);
}

goog.inherits(omm.ui.VideoStrip, goog.ui.Component);

omm.ui.VideoStrip.prototype.createDom = function () {
    this.element_ = soy.renderAsFragment(omm.ui.VideoStripTemplates.container, {localization: this.options_.localization[this.options_.lang]});
    //this.titleElement_ = goog.dom.getElementByClass(goog.getCssName("openmultimedia-componentes-videostrip-title"), this.element_);
    this.contentElement_ = goog.dom.getElementByClass(goog.getCssName("openmultimedia-componentes-videostrip-content"), this.element_);
}

omm.ui.VideoStrip.prototype.getContentElement = function() {
    return this.contentElement_;
}

omm.ui.VideoStrip.prototype.enterDocument = function () {
    goog.DEBUG && console.log("Intalling VideoStrip");

    for ( var i = 0; i < this.controls_.length; i += 1 ) {
        goog.events.listen(this.controls_[i], goog.events.EventType.CHANGE, goog.bind(this.onControlChange_, this));
    }

    goog.base(this, "enterDocument");

    this.reload();
}

omm.ui.VideoStrip.prototype.onControlChange_ = function(event) {
    this.reload();
}

omm.ui.VideoStrip.prototype.reload = function() {
    this.removeChildren(true);

    var apiParams = this.options_.api_params ? goog.object.clone(this.options_.api_params) : {};

    for ( var i = 0; i < this.controls_.length; i += 1 ) {
        goog.object.extend(apiParams, this.controls_[i].getApiParams());
    }

    if ( apiParams['detalle'] != omm.api.DetalleClip.COMPLETO) {
        apiParams['detalle'] = omm.api.DetalleClip.COMPLETO;
    }

    this.manejadorApi_.loadClipList(apiParams, goog.bind(this.onLoadClips_, this));
}

omm.ui.VideoStrip.prototype.onLoadClips_ = function(dataList) {
    goog.DEBUG && console.log("Clips loaded on Ultimos: ", dataList);

    /** @type {omm.ui.VideoStripItem}*/
    var videoItem;
    for ( var i = 0; i < dataList.length; i += 1 ) {
        videoItem = new omm.ui.VideoStripItem(dataList[i], this.getDomHelper());
        this.addChild(videoItem, true);
        goog.events.listen(videoItem, goog.events.EventType.CLICK, this.onItemClick_);
    }
}

omm.ui.VideoStrip.prototype.onItemClick_ = function (event) {
    this.dispatchEvent(new omm.ui.VideoStripEvent(omm.ui.VideoStripEventType.ITEM_CLICK, event.target));
}
