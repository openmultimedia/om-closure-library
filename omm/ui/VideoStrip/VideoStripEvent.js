goog.provide("omm.ui.VideoStripEvent");
goog.provide("omm.ui.VideoStripEventType");

goog.require("goog.events.Event");

omm.ui.VideoStripEvent = function(type, opt_videoStripItem, opt_target) {
    this.item = opt_videoStripItem;
    goog.base(this, type, opt_target);
}

goog.inherits(omm.ui.VideoStripEvent, goog.events.Event);

omm.ui.VideoStripEventType = {
    ITEM_CLICK: "item_click"
};
