goog.provide("omm.ui.VideoStripItem");

goog.require("omm.ui.VideoStripTemplates");
goog.require("omm.utils.TimeFormatter");
goog.require("goog.ui.Component");
goog.require("goog.events.EventType");

omm.ui.VideoStripItem = function (dataItem, opt_domHelper) {
    this.data_ = dataItem;

    this.templateData_ = {
        titulo: dataItem["titulo"],
        thumbnail: dataItem["thumbnail_pequeno"],
        duracion: omm.utils.TimeFormatter.formatDuration(dataItem["duracion"])
    }

    this.onClick_ = goog.bind(this.onClick_, this);

    goog.base(this, opt_domHelper);
}

goog.inherits(omm.ui.VideoStripItem, goog.ui.Component);

omm.ui.VideoStripItem.prototype.getClipData = function() {
    return this.data_;
}

omm.ui.VideoStripItem.prototype.createDom = function() {
    this.element_ = soy.renderAsFragment(omm.ui.VideoStripTemplates.item, this.templateData_);
}

omm.ui.VideoStripItem.prototype.enterDocument = function () {
    goog.events.listen(this.element_, goog.events.EventType.CLICK, this.onClick_);

    goog.base(this, "enterDocument");
}

omm.ui.VideoStripItem.prototype.exitDocument = function () {
    goog.events.unlisten(this.element_, goog.events.EventType.CLICK, this.onClick_);

    goog.base(this, "exitDocument");
}

omm.ui.VideoStripItem.prototype.onClick_ = function (event) {
    this.dispatchEvent(new goog.events.Event(goog.events.EventType.CLICK));
}
