goog.provide("omm.ui.VideoStripOptions");

omm.ui.VideoStripOptions = {
    lang: "",
    video_count: 0,
    controls: null,
    api_params: null,
    localization: null
}

omm.ui.VideoStripLocalization = {
    titulo: "Titulo"
}
