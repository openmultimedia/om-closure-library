goog.provide('omm.ui.WidgetWrapper');

goog.require('goog.style');
goog.require('goog.dom.ViewportSizeMonitor');

omm.ui.WidgetWrapper = function(opt_options, opt_domHelper) {
    var options = opt_options || {};

    this.width_ = options.width || '100%';
    this.height_ = options.height || '100%';
    this.fullscreen_ = options.fullscreen || false;

    this.vsm_ = null;

    goog.base(this, opt_domHelper);
}

goog.inherits(omm.ui.WidgetWrapper, goog.ui.Component);

omm.ui.WidgetWrapper.prototype.getFullscreen = function() {
    return this.fullscreen_;
}

omm.ui.WidgetWrapper.prototype.setFullscreen = function(value) {
    this.fullscreen_ = value;
}

omm.ui.WidgetWrapper.prototype.createDom = function() {
    this.setElementInternal(goog.dom.createDom('div', {'style': 'position: relative;'}));
}

omm.ui.WidgetWrapper.prototype.enterDocument = function() {
    if ( this.fullscreen_ ) {
        var self = this;
        this.vsm_ = new goog.dom.ViewportSizeMonitor();

        function onResize_(event) {
            goog.style.setSize(self.element_, self.vsm_.getSize());
            self.dispatchEvent('resize');
        }

        goog.events.listen(this.vsm_, goog.events.EventType.RESIZE, onResize_);

        onResize_();

        //goog.style.setSize(self.element_, goog.dom.getViewportSize());

    } else {
        this.element_.style.width = this.width_;
        this.element_.style.height = this.height_;
        this.element_.style.overflow = 'hidden';
    }

    goog.base(this, 'enterDocument');
};
