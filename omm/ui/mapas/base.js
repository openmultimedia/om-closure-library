goog.provide('omm.ui.mapas');

/**
 * Tipos de ventana de información
 * @enum {string}
 */
omm.ui.mapas.InfoWindowType = {
  InfoWindow: 'infowindow',
  Overlay: 'overlay'
};