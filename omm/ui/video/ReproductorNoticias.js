goog.provide('omm.ui.video.ReproductorNoticias');

goog.require('goog.ui.Component');
goog.require('omm.ui.video.JwPlugin');
goog.require('omm.configuration');
goog.require('omm.externals.jwplayer');
goog.require('omm.resources.externals.jwplayer');
goog.require('omm.resources.video.jwplugin');
goog.require('goog.object');

omm.externals.jwplayer.requireLib();

/**
 * Wrapper para comunicarse con un reproductor jwPlayer asociado a un elemento
 * de jQuery
 * @param {omm.Medio} medio El medio asociado a este reproductor.
 * @param {Object=} options Opciones de configuración de este reproductor.
 * @param {goog.dom.DomHelper=} opt_domHelper DOM Helper asociado al reproductor.
 * @constructor
 * @extends {goog.ui.Component}
 */
omm.ui.video.ReproductorNoticias = function(medio, options, opt_domHelper) {
  goog.DEBUG && console.groupCollapsed('omm.ui.video.ReproductorNoticias', arguments);

  this.medio_ = medio;

  this.playerWrapperId_ = 'openmultimedia_jwplayer_outside_wrapper_' + goog.getUid(this);

  this.setOptions(options);

  goog.base(this, opt_domHelper);

  goog.DEBUG && console.groupEnd();
};

goog.inherits(omm.ui.video.ReproductorNoticias, goog.ui.Component);

omm.ui.video.ReproductorNoticias.prototype.setOptions = function(options) {
  goog.DEBUG && console.groupCollapsed('omm.ui.video.ReproductorNoticias.prototype.setOptions');

  goog.DEBUG && console.log('Setting options: ', options);

  var newOptions = goog.object.clone(options);

  var newJwplayerOptions = omm.configuration.retrieve(newOptions, 'jwplayerOptions');

  var newMultimediaPluginOptions = omm.configuration.retrieve(newOptions, 'multimediaPluginOptions');

  this.setMainOptions(newOptions);

  this.setJwplayerOptions(newJwplayerOptions);

  this.setMultimediaPluginOptions(newMultimediaPluginOptions);

  goog.DEBUG && console.groupEnd();
};

omm.ui.video.ReproductorNoticias.prototype.setMainOptions = function(options) {
  var fullOptions = omm.configuration.complete(options, this.options_);

  this.options_ = fullOptions;
};

omm.ui.video.ReproductorNoticias.prototype.setJwplayerOptions = function(options) {
  goog.DEBUG && console.groupCollapsed('omm.ui.video.ReproductorNoticias.prototype.setJwplayerOptions', arguments);

  this.jwplayerOptions_ = omm.configuration.complete(options, this.jwplayerOptions_);

  goog.DEBUG && console.groupEnd();
};

omm.ui.video.ReproductorNoticias.prototype.setMultimediaPluginOptions = function(options) {
  goog.DEBUG && console.groupCollapsed('omm.ui.video.ReproductorNoticias.prototype.setMultimediaPluginOptions', arguments);

  var fullOptions = omm.configuration.complete(options, this.multimediaPluginOptions_);

  var newConfig = omm.configuration.complete(fullOptions['config'], this.multimediaPluginOptions_['config']);

  fullOptions['config'] = newConfig;

  this.multimediaPluginOptions_ = fullOptions;

  goog.DEBUG && console.groupEnd();
};

omm.ui.video.ReproductorNoticias.prototype.medio_ = null;

omm.ui.video.ReproductorNoticias.prototype.containerNode_ = null;

omm.ui.video.ReproductorNoticias.prototype.playerReady_ = false;

omm.ui.video.ReproductorNoticias.prototype.pendingClipList_ = null;

omm.ui.video.ReproductorNoticias.prototype.pendingVideoList_ = null;

omm.ui.video.ReproductorNoticias.prototype.pendingAutoplay_ = null;

omm.ui.video.ReproductorNoticias.prototype.options_ = {
  'multimediaPlugin': true,
  'ads' : true
};

omm.ui.video.ReproductorNoticias.prototype.multimediaPluginOptions_ = {
  'name': omm.resources.video.jwplugin.MULTIMEDIA_PLUGIN_NAME,
  'path': omm.resources.video.jwplugin.MULTIMEDIA_PLUGIN_PATH,
  'config': {
    'live': true,
    'liveOptions': {
      'enabled': true,
      'startup': false,
      'position': omm.externals.jwplayer.PluginPosition.Controlbar
    }
  }
};

omm.ui.video.ReproductorNoticias.prototype.jwplayerOptions_ = {
  'skin': omm.resources.externals.jwplayer.skin.GLOW_SKIN,
  'dock': false,
  'debug': 'none',
  'controlbar.position': 'bottom',
  'autostart': false,
  'modes': [
    {'type': 'flash', 'src': omm.resources.externals.jwplayer.SWF_PATH},
    {'type': 'html5'},
    {'type': 'download'}
  ],
  'plugins': {}
};


/**
 * Solicita la carga de un clip de video obtenido del API REST
 * @param {omm.api.Clip} clip Clip de video obtenido del API REST.
 * @param {boolean} autoplay Cuando el video debe reproducirse automáticamente.
 *
omm.ui.video.ReproductorNoticias.prototype.playClip = function (clip, autoplay) {
  goog.DEBUG && console.log("Do Play Clip");
  if ( this.playerReady ) {
    goog.DEBUG && console.log("External load");
    (/** @type {telesur.player.jwplayer.MultimediaPluginInterface} * player.getPlugin("telesur")).loadClip(clip, autoplay);
  } else {
    goog.DEBUG && console.log("External deferred load");
    this.currentVideo = clip
    this.currentAutoplay = autoplay;
  }
};*/

/** @override */
omm.ui.video.ReproductorNoticias.prototype.createDom = function() {
    goog.DEBUG && console.log("DOM CREATE");

    this.element_ = goog.dom.createDom('div', { 'id': this.playerWrapperId_, 'style': 'width: 100%; height: 100%;' });

    this.playerId_ = 'openmultimedia_jwplayer_' + goog.getUid({});

    this.playerElement_ = goog.dom.createDom('div', { 'id': this.playerId_ });

    goog.dom.append(this.element_, this.playerElement_);
};

/** @override */
omm.ui.video.ReproductorNoticias.prototype.enterDocument = function() {
    goog.DEBUG && console.log("ENTER DOCUMENT");
    var jwOptions = this.getJwOptions_();

    goog.DEBUG && console.log('Installing jwplayer in: ', this.playerId_, 'with: ', jwOptions);

    this.player_ = jwplayer(this.playerId_).setup(jwOptions);

    var playerContainer = this.player_.getContainer();

    this.playerElement_ = playerContainer.parentNode;

    this.player_.onReady(goog.partial(omm.ui.video.ReproductorNoticias.onJwplayerReady_, this));

    this.player_.onPlaylist(goog.partial(omm.ui.video.ReproductorNoticias.onJwplayerPlaylist_, this));

    goog.base(this, 'enterDocument');
};

/** @override */
omm.ui.video.ReproductorNoticias.prototype.exitDocument = function() {
  goog.DEBUG && console.log('EXIT Removing player');

  if ( this.playerReady_ ) {
    this.player_.stop();
    //this.player_.remove();
  } else {
    //goog.style.showElement( this.playerElement_, false );
    //this.player_.onReady(function(){ this.remove(); });
  }

  this.player_ = null;
  this.playerReady_ = false;
  this.playerElement_ = null;

  goog.base(this, 'exitDocument');
};

omm.ui.video.ReproductorNoticias.prototype.getJwOptions_ = function() {
  var jwOptions = goog.object.clone(this.jwplayerOptions_);

  if (this.element_) {
    var divSize = goog.style.getSize(this.element_);

    if ( ! ('width' in jwOptions) ) {
      jwOptions['width'] = divSize.width;
    }

    if ( ! ('height' in jwOptions) ) {
      jwOptions['height'] = divSize.height;
    }
  }

  var internalPlugins = {};

  if (this.options_['multimediaPlugin']) {
    internalPlugins[this.multimediaPluginOptions_['path']] = this.multimediaPluginOptions_['config'];
  }

  if ( ! goog.object.isEmpty(internalPlugins) ) {
    jwOptions['plugins'] = omm.configuration.complete(internalPlugins, jwOptions['plugins']);
  }

  return jwOptions;
};

omm.ui.video.ReproductorNoticias.prototype.setPendingClipList_ = function(clipData) {
  this.pendingClipList_ = clipData;
};

omm.ui.video.ReproductorNoticias.prototype.getPendingClipList_ = function() {
  return this.pendingClipList_;
};

omm.ui.video.ReproductorNoticias.prototype.setPendingVideoList_ = function(videoList) {
  this.pendingVideoList_ = videoList;
};

omm.ui.video.ReproductorNoticias.prototype.getPendingVideoList_ = function() {
  return this.pendingVideoList_;
};

omm.ui.video.ReproductorNoticias.prototype.setPendingAutoplay_ = function(autoplay) {
  this.pendingAutoplay_ = autoplay;
};

omm.ui.video.ReproductorNoticias.prototype.getPendingAutoplay_ = function() {
  return this.pendingAutoplay_;
};

omm.ui.video.ReproductorNoticias.prototype.clearPending_ = function() {
  this.pendingAutoplay_ = false;
  this.pendingVideoList_ = null;
  this.pendingClipList_ = null;
}

/** @deprecated Utilizar {@code playClipList} en su lugar*/
omm.ui.video.ReproductorNoticias.prototype.playClip = function(clipData, autoplay) {
  this.playClipList([clipData], autoplay);
};

omm.ui.video.ReproductorNoticias.prototype.playClipList = function(clipData, autoplay) {
  goog.DEBUG && console.log('Playing', clipData);
  this.clearPending_();

  if (this.player_ && this.playerReady_) {
    var renderingMode = this.player_.getRenderingMode();

    if ( renderingMode ) {
      // Se detiene el video actual y se carga el nuevo
      try {
        this.player_.stop();
      } catch (e) {
        goog.DEBUG && console.warn('No se pudo detener el video: ' + e);
      }

      var videoList = omm.ui.video.ReproductorNoticias.makePlaylist( clipData, renderingMode );

      goog.DEBUG && console.log('Playing VideoList', videoList);

      try {
        this.player_.load(videoList);
      } catch(e) {
        goog.DEBUG && console.warn('No se pudo cargar los videos: ' + e);
      }

      // Se conoce el Modo de Rendering actual, se guarda la lista de reproducción procesada en caso de que el player este en modo de espera
      this.setPendingVideoList_(videoList);
    }
  }

  // El reproductor no ha sido inicializado. Se guarda la lista de clips completa.
  this.setPendingClipList_(clipData);

  this.setPendingAutoplay_(autoplay);
};

omm.ui.video.ReproductorNoticias.stop = function() {
  if ( this.player_ ) {
    this.player_.stop();
  }
}

/** @this {jwplayer.Player} */
omm.ui.video.ReproductorNoticias.onJwplayerReady_ = function(multimediaPlayer, event) {
  goog.DEBUG && console.log('External Ready', event);

  multimediaPlayer.playerReady_ = true;

  var pendingVideoList = multimediaPlayer.getPendingVideoList_();

  if ( ! pendingVideoList ) {
    var pendingClipList = multimediaPlayer.getPendingClipList_();

    if ( pendingClipList ) {
      pendingVideoList = omm.ui.video.ReproductorNoticias.makePlaylist(pendingClipList, this.getRenderingMode());
    }
  }

  if (pendingVideoList) {
    goog.DEBUG && console.log('External Queue Process', pendingVideoList);
    this.stop();
    this.load(pendingVideoList);
    multimediaPlayer.setPendingVideoList_(null);
    multimediaPlayer.setPendingClipList_(null);
  }
};

/** @this {jwplayer.Player} */
omm.ui.video.ReproductorNoticias.onJwplayerPlaylist_ = function(multimediaPlayer, event) {
  var autoplay = multimediaPlayer.getPendingAutoplay_();

  if (autoplay) {
    this.play(true);
    multimediaPlayer.setPendingAutoplay_(false);
  }
};

omm.ui.video.ReproductorNoticias.makePlaylist = function (clipList, renderingMode) {
  goog.DEBUG && console.log('Convirtiendo a Playlist', clipList);

  if ( goog.typeOf(clipList) == 'object' ) {
    clipList = [ clipList ];
  }

  var streamingSupport = false;
  streamingSupport = (renderingMode == omm.externals.jwplayer.RenderingMode.Flash);

  var videoList =[];

  var currentClip;

  var video;

  for ( var i = 0; i < clipList.length; i += 1 ) {
    currentClip = clipList[i];

    video = {
        'title': currentClip['titulo'],
        'description': currentClip['descripcion'],
        'duration': currentClip['duracion'],
        'image': currentClip['thumbnail_mediano'],
        'playlist.image': currentClip['thumbnail_pequeno']
    };

    if ( streamingSupport && ( currentClip['metodo_preferido'] == omm.api.MetodoTransmision.STREAMING ) ) {
      video['streamer'] = currentClip['streaming']['rtmp_server'];
      video['file'] = currentClip['streaming']['rtmp_file'];
    } else {
      video['file'] = currentClip['archivo_url'];
      video['provider']  = 'http';
      video['http.startparam'] = 'start';
    }

    videoList[i] = video;
  }

  goog.DEBUG && console.log('Playlist', videoList);

  return videoList;
};
