goog.provide('omm.uri');

goog.require('goog.Uri');

/**
 * Resuelve desde la URL actual la URI a un recurso. Se pueden indicar dos URIs
 * diferentes si se require diferenciar entre una ruta para protocolo HTTP y el
 * protocolo HTTPS
 * @param {string} httpUrl URI relativa a resolver, o uri HTTP si se require
 *                         sólo la diferenciación entre https y http
 * @param {string=} opt_httpsUrl URI HTTPS a seleccionar para  diferenciar http
 *                               y https
 * @return {goog.Uri} Uri absoluta al recurso
 * @deprecated Use {@code omm.uri.resolveUrl} instead.
 */
omm.uri.resolve = function(httpUrl, opt_httpsUrl) {

    if (! goog.isDef(omm.uri.baseUri_)) {
      omm.uri.baseUri_ = new goog.Uri(self.location);
    }

    var uriToResolve;

    if (omm.uri.baseUri_.getScheme() == 'https') {
      uriToResolve = goog.isDef(opt_httpsUrl) ? opt_httpsUrl : httpUrl;
    } else if (omm.uri.baseUri_.getScheme() == 'http') {
      uriToResolve = httpUrl;
    } else {
      throw new Error('Scheme not supported to resolve');
    }

    return omm.uri.baseUri_.resolve(new goog.Uri(uriToResolve));
};

/**
 * Resolves the absolute URL of a resource relative to the current URL.
 *
 * If you pass both an HTTPS and HTTP urls, the HTTP one will be used when
 * resolving from an HTTP page, and the HTTPS one will be used in any
 * other context
 * @param  {string} httpsUrl
 * @param  {string} opt_httpUrl
 * @return {goog.Uri}
 */
omm.uri.resolveUrl = function(httpsUrl, opt_httpUrl) {
  if (! goog.isDef(omm.uri.baseUri_))
    omm.uri.baseUri_ = new goog.Uri(self.location);

  var uriToResolve =
    (opt_httpUrl && omm.uri.baseUri_.getScheme() == 'http') ? opt_httpUrl : httpsUrl;

  return omm.uri.baseUri_.resolve(new goog.Uri(uriToResolve));
};
