goog.provide('omm.utils');

goog.require('goog.object');

/** @deprecated */
omm.utils.completeOptions = function (defaultOptions, partialOptions) {
  var fullOptions = goog.object.clone(defaultOptions);
  if ( typeof partialOptions == 'object' ) {
    goog.object.extend(fullOptions, partialOptions);
  }
  return fullOptions;
};

/** @deprecated */
omm.utils.retrieveSubOptions = function (mainOptions, subOptionsKey) {
  var subOptions = false;
  if ( subOptionsKey in mainOptions ) {
    subOptions = mainOptions[subOptionsKey];
    delete mainOptions[subOptionsKey];
  }
  return subOptions;
}